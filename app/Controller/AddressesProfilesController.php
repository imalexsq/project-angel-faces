<?php
App::uses('AppController', 'Controller');
/**
 * AddressesProfiles Controller
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Controllers
 * @since         PAF v 1.0
 *
 *
 * @property AddressesProfile $AddressesProfile
 */
class AddressesProfilesController extends AppController
{

/**
 * index method
 *
 * @return void
 */
	public function index()
	{
		$this->AddressesProfile->recursive = 0;
		$this->set('addressesProfiles', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null)
	{
		$this->AddressesProfile->id = $id;
		if (!$this->AddressesProfile->exists())
		{
			throw new NotFoundException(__('Invalid addresses profile'));
		}
		$this->set('addressesProfile', $this->AddressesProfile->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add()
	{
		if ($this->request->is('post'))
		{
			$this->AddressesProfile->create();
			if ($this->AddressesProfile->save($this->request->data))
			{
				$this->Session->setFlash(__('The addresses profile has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The addresses profile could not be saved. Please, try again.'));
			}
		}
		$profiles = $this->AddressesProfile->Profile->find('list');
		$addresses = $this->AddressesProfile->Address->find('list');
		$this->set(compact('profiles', 'addresses'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null)
	{
		$this->AddressesProfile->id = $id;
		if (!$this->AddressesProfile->exists())
		{
			throw new NotFoundException(__('Invalid addresses profile'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->AddressesProfile->save($this->request->data))
			{
				$this->Session->setFlash(__('The addresses profile has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The addresses profile could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->AddressesProfile->read(null, $id);
		}
		$profiles = $this->AddressesProfile->Profile->find('list');
		$addresses = $this->AddressesProfile->Address->find('list');
		$this->set(compact('profiles', 'addresses'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->AddressesProfile->id = $id;
		if (!$this->AddressesProfile->exists())
		{
			throw new NotFoundException(__('Invalid addresses profile'));
		}
		if ($this->AddressesProfile->delete())
		{
			$this->Session->setFlash(__('Addresses profile deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Addresses profile was not deleted'));
		$this->redirect(array('action' => 'index'));
	}


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index()
	{
		$this->AddressesProfile->recursive = 0;
		$this->set('addressesProfiles', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null)
	{
		$this->AddressesProfile->id = $id;
		if (!$this->AddressesProfile->exists())
		{
			throw new NotFoundException(__('Invalid addresses profile'));
		}
		$this->set('addressesProfile', $this->AddressesProfile->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add()
	{
		if ($this->request->is('post'))
		{
			$this->AddressesProfile->create();
			if ($this->AddressesProfile->save($this->request->data))
			{
				$this->Session->setFlash(__('The addresses profile has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The addresses profile could not be saved. Please, try again.'));
			}
		}
		$profiles = $this->AddressesProfile->Profile->find('list');
		$addresses = $this->AddressesProfile->Address->find('list');
		$this->set(compact('profiles', 'addresses'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null)
	{
		$this->AddressesProfile->id = $id;
		if (!$this->AddressesProfile->exists())
		{
			throw new NotFoundException(__('Invalid addresses profile'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->AddressesProfile->save($this->request->data))
			{
				$this->Session->setFlash(__('The addresses profile has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The addresses profile could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->AddressesProfile->read(null, $id);
		}
		$profiles = $this->AddressesProfile->Profile->find('list');
		$addresses = $this->AddressesProfile->Address->find('list');
		$this->set(compact('profiles', 'addresses'));
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->AddressesProfile->id = $id;
		if (!$this->AddressesProfile->exists())
		{
			throw new NotFoundException(__('Invalid addresses profile'));
		}
		if ($this->AddressesProfile->delete())
		{
			$this->Session->setFlash(__('Addresses profile deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Addresses profile was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
