<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
    public $helpers = array(
        'Session',
        'Html' => array('className' => 'TwitterBootstrap.BootstrapHtml'),
        'Form' => array('className' => 'TwitterBootstrap.BootstrapForm'),
        'Paginator' => array('className' => 'TwitterBootstrap.BootstrapPaginator'),
        'Js' => array('Jquery'),
        'TinyMCE.TinyMCE'
    );

	public $components = array(
		'Acl',
		'Auth' => array(
			'authorize' => array(
				'Actions' => array('actionPath' => 'controllers')
			)
		),
		'DebugKit.Toolbar',
		'Session'
	);

	public function beforeFilter()
	{
		//Configure AuthComponent
		$this->Auth->loginAction = array('controller' => 'users', 'action' => 'login');
		$this->Auth->logoutRedirect = array('controller' => 'users', 'action' => 'login');
		$this->Auth->loginRedirect = array('controller' => 'pages', 'action' => 'display', 'home');
        $this->Auth->allow(array('display','listindex', 'show', 'send'));


        $this->set('authUser', $this->Auth->user());

        /**
         * Used to clear the cache i.e. http://paf.t73.biz/?emptycache
         */
        if(isset($this->params['url']['emptycache'])) {
            // clear Cache::write() items
            Cache::clear();
            // clear core cache
            $cachePaths = array('views', 'persistent', 'models');
            foreach($cachePaths as $config) {
                clearCache(null, $config);
            }

            if(function_exists('apc_clear_cache'))
            {
                apc_clear_cache();
            }

            $this->Session->setFlash('Cache cleared', 'default', array(), 'info');
        }
	}

    public function beforeRender()
    {
        $default = array('display', 'login', 'listindex', 'show', 'send');
        if(isset($this->request->params['admin']) && $this->Auth->user() != null)
        {
            $this->layout = 'admin';
        }
        elseif (in_array($this->request->params['action'], $default) || $this->Auth->user() == null)
        {
            $this->layout = 'default';
        }
        else
        {
            $this->layout = 'manage';
        }

    }

}
