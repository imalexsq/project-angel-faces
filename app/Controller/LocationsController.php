<?php
App::uses('AppController', 'Controller');
/**
 * Locations Controller
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Controllers
 * @since         PAF v 1.0
 *
 *
 * @property Location $Location
 */
class LocationsController extends AppController
{

/**
 * index method
 *
 * @return void
 */
	public function index()
	{
		$this->Location->recursive = 0;
		$this->set('locations', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null)
	{
		$this->Location->id = $id;
		if (!$this->Location->exists())
		{
			throw new NotFoundException(__('Invalid location'));
		}
		$this->set('location', $this->Location->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add()
	{
		if ($this->request->is('post'))
		{
			$this->Location->create();
			if ($this->Location->save($this->request->data))
			{
				$this->Session->setFlash(__('The location has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The location could not be saved. Please, try again.'));
			}
		}
		$projects = $this->Location->Project->find('list');
		$profiles = $this->Location->Profile->find('list');
		$addresses = $this->Location->Address->find('list');
		$fees = $this->Location->Fee->find('list');
		$this->set(compact('projects', 'programs', 'profiles', 'addresses', 'fees'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null)
	{
		$this->Location->id = $id;
		if (!$this->Location->exists())
		{
			throw new NotFoundException(__('Invalid location'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->Location->save($this->request->data))
			{
				$this->Session->setFlash(__('The location has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The location could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->Location->read(null, $id);
		}
		$projects = $this->Location->Project->find('list');
		$profiles = $this->Location->Profile->find('list');
		$addresses = $this->Location->Address->find('list');
		$fees = $this->Location->Fee->find('list');
		$this->set(compact('projects', 'programs', 'profiles', 'addresses', 'fees'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->Location->id = $id;
		if (!$this->Location->exists())
		{
			throw new NotFoundException(__('Invalid location'));
		}
		if ($this->Location->delete())
		{
			$this->Session->setFlash(__('Location deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Location was not deleted'));
		$this->redirect(array('action' => 'index'));
	}


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index()
	{
		$this->Location->recursive = 0;
		$this->set('locations', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null)
	{
		$this->Location->id = $id;
		if (!$this->Location->exists())
		{
			throw new NotFoundException(__('Invalid location'));
		}
		$this->set('location', $this->Location->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add()
	{
		if ($this->request->is('post'))
		{
			$this->Location->create();
			if ($this->Location->save($this->request->data))
			{
				$this->Session->setFlash(__('The location has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The location could not be saved. Please, try again.'));
			}
		}
		$projects = $this->Location->Project->find('list');
		$profiles = $this->Location->Profile->find('list');
		$addresses = $this->Location->Address->find('list');
		$fees = $this->Location->Fee->find('list');
		$this->set(compact('projects', 'programs', 'profiles', 'addresses', 'fees'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null)
	{
		$this->Location->id = $id;
		if (!$this->Location->exists())
		{
			throw new NotFoundException(__('Invalid location'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->Location->save($this->request->data))
			{
				$this->Session->setFlash(__('The location has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The location could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->Location->read(null, $id);
		}
		$projects = $this->Location->Project->find('list');
		$profiles = $this->Location->Profile->find('list');
		$addresses = $this->Location->Address->find('list');
		$fees = $this->Location->Fee->find('list');
		$this->set(compact('projects', 'programs', 'profiles', 'addresses', 'fees'));
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->Location->id = $id;
		if (!$this->Location->exists())
		{
			throw new NotFoundException(__('Invalid location'));
		}
		if ($this->Location->delete())
		{
			$this->Session->setFlash(__('Location deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Location was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
