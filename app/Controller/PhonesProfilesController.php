<?php
App::uses('AppController', 'Controller');
/**
 * PhonesProfiles Controller
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Controllers
 * @since         PAF v 1.0
 *
 *
 * @property PhonesProfile $PhonesProfile
 */
class PhonesProfilesController extends AppController
{

/**
 * index method
 *
 * @return void
 */
	public function index()
	{
		$this->PhonesProfile->recursive = 0;
		$this->set('phonesProfiles', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null)
	{
		$this->PhonesProfile->id = $id;
		if (!$this->PhonesProfile->exists())
		{
			throw new NotFoundException(__('Invalid phones profile'));
		}
		$this->set('phonesProfile', $this->PhonesProfile->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add()
	{
		if ($this->request->is('post'))
		{
			$this->PhonesProfile->create();
			if ($this->PhonesProfile->save($this->request->data))
			{
				$this->Session->setFlash(__('The phones profile has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The phones profile could not be saved. Please, try again.'));
			}
		}
		$profiles = $this->PhonesProfile->Profile->find('list');
		$phones = $this->PhonesProfile->Phone->find('list');
		$this->set(compact('profiles', 'phones'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null)
	{
		$this->PhonesProfile->id = $id;
		if (!$this->PhonesProfile->exists())
		{
			throw new NotFoundException(__('Invalid phones profile'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->PhonesProfile->save($this->request->data))
			{
				$this->Session->setFlash(__('The phones profile has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The phones profile could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->PhonesProfile->read(null, $id);
		}
		$profiles = $this->PhonesProfile->Profile->find('list');
		$phones = $this->PhonesProfile->Phone->find('list');
		$this->set(compact('profiles', 'phones'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->PhonesProfile->id = $id;
		if (!$this->PhonesProfile->exists())
		{
			throw new NotFoundException(__('Invalid phones profile'));
		}
		if ($this->PhonesProfile->delete())
		{
			$this->Session->setFlash(__('Phones profile deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Phones profile was not deleted'));
		$this->redirect(array('action' => 'index'));
	}


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index()
	{
		$this->PhonesProfile->recursive = 0;
		$this->set('phonesProfiles', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null)
	{
		$this->PhonesProfile->id = $id;
		if (!$this->PhonesProfile->exists())
		{
			throw new NotFoundException(__('Invalid phones profile'));
		}
		$this->set('phonesProfile', $this->PhonesProfile->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add()
	{
		if ($this->request->is('post'))
		{
			$this->PhonesProfile->create();
			if ($this->PhonesProfile->save($this->request->data))
			{
				$this->Session->setFlash(__('The phones profile has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The phones profile could not be saved. Please, try again.'));
			}
		}
		$profiles = $this->PhonesProfile->Profile->find('list');
		$phones = $this->PhonesProfile->Phone->find('list');
		$this->set(compact('profiles', 'phones'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null)
	{
		$this->PhonesProfile->id = $id;
		if (!$this->PhonesProfile->exists())
		{
			throw new NotFoundException(__('Invalid phones profile'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->PhonesProfile->save($this->request->data))
			{
				$this->Session->setFlash(__('The phones profile has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The phones profile could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->PhonesProfile->read(null, $id);
		}
		$profiles = $this->PhonesProfile->Profile->find('list');
		$phones = $this->PhonesProfile->Phone->find('list');
		$this->set(compact('profiles', 'phones'));
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->PhonesProfile->id = $id;
		if (!$this->PhonesProfile->exists())
		{
			throw new NotFoundException(__('Invalid phones profile'));
		}
		if ($this->PhonesProfile->delete())
		{
			$this->Session->setFlash(__('Phones profile deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Phones profile was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
