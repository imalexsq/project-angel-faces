<?php
App::uses('AppController', 'Controller');
/**
 * Inventories Controller
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Controllers
 * @since         PAF v 1.0
 *
 *
 * @property Inventory $Inventory
 */
class InventoriesController extends AppController
{

/**
 * index method
 *
 * @return void
 */
	public function index()
	{
		$this->Inventory->recursive = 0;
		$this->set('inventories', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null)
	{
		$this->Inventory->id = $id;
		if (!$this->Inventory->exists())
		{
			throw new NotFoundException(__('Invalid inventory'));
		}
		$this->set('inventory', $this->Inventory->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add()
	{
		if ($this->request->is('post'))
		{
			$this->Inventory->create();
			if ($this->Inventory->save($this->request->data))
			{
				$this->Session->setFlash(__('The inventory has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The inventory could not be saved. Please, try again.'));
			}
		}
		$locations = $this->Inventory->Location->find('list');
		$yieldtypes = $this->Inventory->Yieldtype->find('list');
		$yieldMeasures = $this->Inventory->YieldMeasure->find('list');
		$this->set(compact('locations', 'yieldtypes', 'yieldMeasures'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null)
	{
		$this->Inventory->id = $id;
		if (!$this->Inventory->exists())
		{
			throw new NotFoundException(__('Invalid inventory'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->Inventory->save($this->request->data))
			{
				$this->Session->setFlash(__('The inventory has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The inventory could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->Inventory->read(null, $id);
		}
		$locations = $this->Inventory->Location->find('list');
		$yieldtypes = $this->Inventory->Yieldtype->find('list');
		$yieldMeasures = $this->Inventory->YieldMeasure->find('list');
		$this->set(compact('locations', 'yieldtypes', 'yieldMeasures'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->Inventory->id = $id;
		if (!$this->Inventory->exists())
		{
			throw new NotFoundException(__('Invalid inventory'));
		}
		if ($this->Inventory->delete())
		{
			$this->Session->setFlash(__('Inventory deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Inventory was not deleted'));
		$this->redirect(array('action' => 'index'));
	}


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index()
	{
		$this->Inventory->recursive = 0;
		$this->set('inventories', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null)
	{
		$this->Inventory->id = $id;
		if (!$this->Inventory->exists())
		{
			throw new NotFoundException(__('Invalid inventory'));
		}
		$this->set('inventory', $this->Inventory->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add()
	{
		if ($this->request->is('post'))
		{
			$this->Inventory->create();
			if ($this->Inventory->save($this->request->data))
			{
				$this->Session->setFlash(__('The inventory has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The inventory could not be saved. Please, try again.'));
			}
		}
		$locations = $this->Inventory->Location->find('list');
		$yieldtypes = $this->Inventory->Yieldtype->find('list');
		$yieldMeasures = $this->Inventory->YieldMeasure->find('list');
		$this->set(compact('locations', 'yieldtypes', 'yieldMeasures'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null)
	{
		$this->Inventory->id = $id;
		if (!$this->Inventory->exists())
		{
			throw new NotFoundException(__('Invalid inventory'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->Inventory->save($this->request->data))
			{
				$this->Session->setFlash(__('The inventory has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The inventory could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->Inventory->read(null, $id);
		}
		$locations = $this->Inventory->Location->find('list');
		$yieldtypes = $this->Inventory->Yieldtype->find('list');
		$yieldMeasures = $this->Inventory->YieldMeasure->find('list');
		$this->set(compact('locations', 'yieldtypes', 'yieldMeasures'));
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->Inventory->id = $id;
		if (!$this->Inventory->exists())
		{
			throw new NotFoundException(__('Invalid inventory'));
		}
		if ($this->Inventory->delete())
		{
			$this->Session->setFlash(__('Inventory deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Inventory was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
