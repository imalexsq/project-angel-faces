<?php
App::uses('AppController', 'Controller');
/**
 * Schedules Controller
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Controllers
 * @since         PAF v 1.0
 *
 *
 * @property Schedule $Schedule
 */
class SchedulesController extends AppController
{

/**
 * index method
 *
 * @return void
 */
	public function index()
	{
		$this->Schedule->recursive = 0;
		$this->set('schedules', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null)
	{
		$this->Schedule->id = $id;
		if (!$this->Schedule->exists())
		{
			throw new NotFoundException(__('Invalid schedule'));
		}
		$this->set('schedule', $this->Schedule->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add()
	{
		if ($this->request->is('post'))
		{
			$this->Schedule->create();
			if ($this->Schedule->save($this->request->data))
			{
				$this->Session->setFlash(__('The schedule has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The schedule could not be saved. Please, try again.'));
			}
		}
		$events = $this->Schedule->Event->find('list');
		$functiontypes = $this->Schedule->Functiontype->find('list');
		$profiles = $this->Schedule->Profile->find('list');
		$scheduletypes = $this->Schedule->Scheduletype->find('list');
		$this->set(compact('events', 'functiontypes', 'profiles', 'scheduletypes'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null)
	{
		$this->Schedule->id = $id;
		if (!$this->Schedule->exists())
		{
			throw new NotFoundException(__('Invalid schedule'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->Schedule->save($this->request->data))
			{
				$this->Session->setFlash(__('The schedule has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The schedule could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->Schedule->read(null, $id);
		}
		$events = $this->Schedule->Event->find('list');
		$functiontypes = $this->Schedule->Functiontype->find('list');
		$profiles = $this->Schedule->Profile->find('list');
		$scheduletypes = $this->Schedule->Scheduletype->find('list');
		$this->set(compact('events', 'functiontypes', 'profiles', 'scheduletypes'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->Schedule->id = $id;
		if (!$this->Schedule->exists())
		{
			throw new NotFoundException(__('Invalid schedule'));
		}
		if ($this->Schedule->delete())
		{
			$this->Session->setFlash(__('Schedule deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Schedule was not deleted'));
		$this->redirect(array('action' => 'index'));
	}


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index()
	{
		$this->Schedule->recursive = 0;
		$this->set('schedules', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null)
	{
		$this->Schedule->id = $id;
		if (!$this->Schedule->exists())
		{
			throw new NotFoundException(__('Invalid schedule'));
		}
		$this->set('schedule', $this->Schedule->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add()
	{
		if ($this->request->is('post'))
		{
			$this->Schedule->create();
			if ($this->Schedule->save($this->request->data))
			{
				$this->Session->setFlash(__('The schedule has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The schedule could not be saved. Please, try again.'));
			}
		}
		$events = $this->Schedule->Event->find('list');
		$functiontypes = $this->Schedule->Functiontype->find('list');
		$profiles = $this->Schedule->Profile->find('list');
		$scheduletypes = $this->Schedule->Scheduletype->find('list');
		$this->set(compact('events', 'functiontypes', 'profiles', 'scheduletypes'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null)
	{
		$this->Schedule->id = $id;
		if (!$this->Schedule->exists())
		{
			throw new NotFoundException(__('Invalid schedule'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->Schedule->save($this->request->data))
			{
				$this->Session->setFlash(__('The schedule has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The schedule could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->Schedule->read(null, $id);
		}
		$events = $this->Schedule->Event->find('list');
		$functiontypes = $this->Schedule->Functiontype->find('list');
		$profiles = $this->Schedule->Profile->find('list');
		$scheduletypes = $this->Schedule->Scheduletype->find('list');
		$this->set(compact('events', 'functiontypes', 'profiles', 'scheduletypes'));
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->Schedule->id = $id;
		if (!$this->Schedule->exists())
		{
			throw new NotFoundException(__('Invalid schedule'));
		}
		if ($this->Schedule->delete())
		{
			$this->Session->setFlash(__('Schedule deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Schedule was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
