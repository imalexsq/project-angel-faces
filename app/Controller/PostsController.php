<?php

App::uses('AppController', 'Controller');
/**
 * Posts Controller
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Controllers
 * @since         PAF v 1.0
 *
 *
 * @property Post $Post
 */

class PostsController extends AppController
{

    /**
     * index method
     *
     * @return void
     */
    public function index()
    {
        $this->Post->recursive = 0;
        $this->set('posts', $this->paginate());
    }

    /**
     * list method
     *
     * @return void
     */
    public function listindex()
    {
        $this->Post->recursive = 0;
        $this->set('posts', $this->paginate());
    }

    /**
     * show method
     *
     *
     * @param null $slug
     * @throws NotFoundException
     * @internal param string $id
     * @return void
     */
    public function show($slug = null)
    {
        $post = $this->Post->findBySlug($slug);
        if(empty($slug))
        {
            throw new NotFoundException(__('Invalid post'));
        }
        $this->set('post', $post);
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null)
    {
        $this->Post->id = $id;
        if (!$this->Post->exists())
        {
            throw new NotFoundException(__('Invalid post'));
        }
        $this->set('post', $this->Post->read(null, $id));
    }

    /**
 * add method
 *
 * @return void
 */
	public function add()
	{
		if ($this->request->is('post'))
		{
			$this->Post->create();
			if ($this->Post->save($this->request->data))
			{
				$this->Session->setFlash(__('The post has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The post could not be saved. Please, try again.'));
			}
		}
		$keywords = $this->Post->Keyword->find('list');
		$tags = $this->Post->Tag->find('list');
		$this->set(compact('keywords', 'tags'));

	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null)
	{
		$this->Post->id = $id;
		if (!$this->Post->exists())
		{
			throw new NotFoundException(__('Invalid post'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->Post->save($this->request->data))
			{
				$this->Session->setFlash(__('The post has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The post could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->Post->read(null, $id);
		}
		$keywords = $this->Post->Keyword->find('list');
		$tags = $this->Post->Tag->find('list');
		$this->set(compact('keywords', 'tags'));

	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->Post->id = $id;
		if (!$this->Post->exists())
		{
			throw new NotFoundException(__('Invalid post'));
		}
		if ($this->Post->delete())
		{
			$this->Session->setFlash(__('Post deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Post was not deleted'));
		$this->redirect(array('action' => 'index'));
	}


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index()
	{
		$this->Post->recursive = 0;
		$this->set('posts', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null)
	{
		$this->Post->id = $id;
		if (!$this->Post->exists())
		{
			throw new NotFoundException(__('Invalid post'));
		}
		$this->set('post', $this->Post->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add()
	{
		if ($this->request->is('post'))
		{
			$this->Post->create();
			if ($this->Post->save($this->request->data))
			{
				$this->Session->setFlash(__('The post has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The post could not be saved. Please, try again.'));
			}
		}
		$keywords = $this->Post->Keyword->find('list');
		$tags = $this->Post->Tag->find('list');
		$this->set(compact('keywords', 'tags'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null)
	{
		$this->Post->id = $id;
		if (!$this->Post->exists())
		{
			throw new NotFoundException(__('Invalid post'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->Post->save($this->request->data))
			{
				$this->Session->setFlash(__('The post has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The post could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->Post->read(null, $id);
		}
		$keywords = $this->Post->Keyword->find('list');
		$tags = $this->Post->Tag->find('list');
		$this->set(compact('keywords', 'tags'));
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->Post->id = $id;
		if (!$this->Post->exists())
		{
			throw new NotFoundException(__('Invalid post'));
		}
		if ($this->Post->delete())
		{
			$this->Session->setFlash(__('Post deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Post was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
