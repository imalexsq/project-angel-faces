<?php
App::uses('AppController', 'Controller');
/**
 * Expensetypes Controller
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Controllers
 * @since         PAF v 1.0
 *
 *
 * @property Expensetype $Expensetype
 */
class ExpensetypesController extends AppController
{

/**
 * index method
 *
 * @return void
 */
	public function index()
	{
		$this->Expensetype->recursive = 0;
		$this->set('expensetypes', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null)
	{
		$this->Expensetype->id = $id;
		if (!$this->Expensetype->exists())
		{
			throw new NotFoundException(__('Invalid expensetype'));
		}
		$this->set('expensetype', $this->Expensetype->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add()
	{
		if ($this->request->is('post'))
		{
			$this->Expensetype->create();
			if ($this->Expensetype->save($this->request->data))
			{
				$this->Session->setFlash(__('The expensetype has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The expensetype could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null)
	{
		$this->Expensetype->id = $id;
		if (!$this->Expensetype->exists())
		{
			throw new NotFoundException(__('Invalid expensetype'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->Expensetype->save($this->request->data))
			{
				$this->Session->setFlash(__('The expensetype has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The expensetype could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->Expensetype->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->Expensetype->id = $id;
		if (!$this->Expensetype->exists())
		{
			throw new NotFoundException(__('Invalid expensetype'));
		}
		if ($this->Expensetype->delete())
		{
			$this->Session->setFlash(__('Expensetype deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Expensetype was not deleted'));
		$this->redirect(array('action' => 'index'));
	}


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index()
	{
		$this->Expensetype->recursive = 0;
		$this->set('expensetypes', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null)
	{
		$this->Expensetype->id = $id;
		if (!$this->Expensetype->exists())
		{
			throw new NotFoundException(__('Invalid expensetype'));
		}
		$this->set('expensetype', $this->Expensetype->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add()
	{
		if ($this->request->is('post'))
		{
			$this->Expensetype->create();
			if ($this->Expensetype->save($this->request->data))
			{
				$this->Session->setFlash(__('The expensetype has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The expensetype could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null)
	{
		$this->Expensetype->id = $id;
		if (!$this->Expensetype->exists())
		{
			throw new NotFoundException(__('Invalid expensetype'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->Expensetype->save($this->request->data))
			{
				$this->Session->setFlash(__('The expensetype has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The expensetype could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->Expensetype->read(null, $id);
		}
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->Expensetype->id = $id;
		if (!$this->Expensetype->exists())
		{
			throw new NotFoundException(__('Invalid expensetype'));
		}
		if ($this->Expensetype->delete())
		{
			$this->Session->setFlash(__('Expensetype deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Expensetype was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
