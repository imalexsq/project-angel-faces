<?php
App::uses('AppController', 'Controller');
/**
 * Events Controller
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Controllers
 * @since         PAF v 1.0
 *
 *
 * @property Event $Event
 */
class EventsController extends AppController
{

/**
 * index method
 *
 * @return void
 */
	public function index()
	{
		$this->Event->recursive = 0;
		$this->set('events', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null)
	{
		$this->Event->id = $id;
		if (!$this->Event->exists())
		{
			throw new NotFoundException(__('Invalid event'));
		}
		$this->set('event', $this->Event->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add()
	{
		if ($this->request->is('post'))
		{
			$this->Event->create();
			if ($this->Event->save($this->request->data))
			{
				$this->Session->setFlash(__('The event has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The event could not be saved. Please, try again.'));
			}
		}
		$locations = $this->Event->Location->find('list');
		$projects = $this->Event->Project->find('list');
		$this->set(compact('locations', 'programs', 'projects'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null)
	{
		$this->Event->id = $id;
		if (!$this->Event->exists())
		{
			throw new NotFoundException(__('Invalid event'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->Event->save($this->request->data))
			{
				$this->Session->setFlash(__('The event has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The event could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->Event->read(null, $id);
		}
		$locations = $this->Event->Location->find('list');
		$projects = $this->Event->Project->find('list');
		$this->set(compact('locations', 'programs', 'projects'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->Event->id = $id;
		if (!$this->Event->exists())
		{
			throw new NotFoundException(__('Invalid event'));
		}
		if ($this->Event->delete())
		{
			$this->Session->setFlash(__('Event deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Event was not deleted'));
		$this->redirect(array('action' => 'index'));
	}


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index()
	{
		$this->Event->recursive = 0;
		$this->set('events', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null)
	{
		$this->Event->id = $id;
		if (!$this->Event->exists())
		{
			throw new NotFoundException(__('Invalid event'));
		}
		$this->set('event', $this->Event->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add()
	{
		if ($this->request->is('post'))
		{
			$this->Event->create();
			if ($this->Event->save($this->request->data))
			{
				$this->Session->setFlash(__('The event has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The event could not be saved. Please, try again.'));
			}
		}
		$locations = $this->Event->Location->find('list');
		$projects = $this->Event->Project->find('list');
		$this->set(compact('locations', 'programs', 'projects'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null)
	{
		$this->Event->id = $id;
		if (!$this->Event->exists())
		{
			throw new NotFoundException(__('Invalid event'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->Event->save($this->request->data))
			{
				$this->Session->setFlash(__('The event has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The event could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->Event->read(null, $id);
		}
		$locations = $this->Event->Location->find('list');
		$projects = $this->Event->Project->find('list');
		$this->set(compact('locations', 'programs', 'projects'));
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->Event->id = $id;
		if (!$this->Event->exists())
		{
			throw new NotFoundException(__('Invalid event'));
		}
		if ($this->Event->delete())
		{
			$this->Session->setFlash(__('Event deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Event was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
