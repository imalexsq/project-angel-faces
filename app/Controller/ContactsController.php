<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * Contacts Controller
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Controllers
 * @since         PAF v 1.0
 *
 *
 * @property Address $Address
 */
class ContactsController extends AppController
{
    public $uses = array();

    public function send()
    {
        $Email = new CakeEmail();
        $Email->config('smtp');
        $Email->to('rhondak@projectangelfaces.org');
        $Email->subject('Contact from ' . $this->request->data['Contact']['email']);
        $Email->send($this->request->data['Contact']['comment']);
    }
}
