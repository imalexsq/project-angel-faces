<?php
App::uses('AppController', 'Controller');
/**
 * Teams Controller
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Controllers
 * @since         PAF v 1.0
 *
 *
 * @property Team $Team
 */
class TeamsController extends AppController
{

/**
 * index method
 *
 * @return void
 */
	public function index()
	{
		$this->Team->recursive = 0;
		$this->set('teams', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null)
	{
		$this->Team->id = $id;
		if (!$this->Team->exists())
		{
			throw new NotFoundException(__('Invalid team'));
		}
		$this->set('team', $this->Team->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add()
	{
		if ($this->request->is('post'))
		{
			$this->Team->create();
			if ($this->Team->save($this->request->data))
			{
				$this->Session->setFlash(__('The team has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The team could not be saved. Please, try again.'));
			}
		}
		$teamtypes = $this->Team->Teamtype->find('list');
		$profiles = $this->Team->Profile->find('list');
		$this->set(compact('teamtypes', 'profiles'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null)
	{
		$this->Team->id = $id;
		if (!$this->Team->exists())
		{
			throw new NotFoundException(__('Invalid team'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->Team->save($this->request->data))
			{
				$this->Session->setFlash(__('The team has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The team could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->Team->read(null, $id);
		}
		$teamtypes = $this->Team->Teamtype->find('list');
		$profiles = $this->Team->Profile->find('list');
		$this->set(compact('teamtypes', 'profiles'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->Team->id = $id;
		if (!$this->Team->exists())
		{
			throw new NotFoundException(__('Invalid team'));
		}
		if ($this->Team->delete())
		{
			$this->Session->setFlash(__('Team deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Team was not deleted'));
		$this->redirect(array('action' => 'index'));
	}


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index()
	{
		$this->Team->recursive = 0;
		$this->set('teams', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null)
	{
		$this->Team->id = $id;
		if (!$this->Team->exists())
		{
			throw new NotFoundException(__('Invalid team'));
		}
		$this->set('team', $this->Team->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add()
	{
		if ($this->request->is('post'))
		{
			$this->Team->create();
			if ($this->Team->save($this->request->data))
			{
				$this->Session->setFlash(__('The team has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The team could not be saved. Please, try again.'));
			}
		}
		$teamtypes = $this->Team->Teamtype->find('list');
		$profiles = $this->Team->Profile->find('list');
		$this->set(compact('teamtypes', 'profiles'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null)
	{
		$this->Team->id = $id;
		if (!$this->Team->exists())
		{
			throw new NotFoundException(__('Invalid team'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->Team->save($this->request->data))
			{
				$this->Session->setFlash(__('The team has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The team could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->Team->read(null, $id);
		}
		$teamtypes = $this->Team->Teamtype->find('list');
		$profiles = $this->Team->Profile->find('list');
		$this->set(compact('teamtypes', 'profiles'));
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->Team->id = $id;
		if (!$this->Team->exists())
		{
			throw new NotFoundException(__('Invalid team'));
		}
		if ($this->Team->delete())
		{
			$this->Session->setFlash(__('Team deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Team was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
