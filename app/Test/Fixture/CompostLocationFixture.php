<?php
/**
 * CompostLocationFixture
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * CakeBizBase : Rapid Development Framework for eBiz Results (http://cakebizbase.ebizresults.com)
 * Copyright 2013, eBiz Results LLC. (http://ebizresults.com)
 *
 *
 * @copyright     Copyright 2013, eBiz Results LLC. (http://ebizresults.com)
 * @link          http://cakebizbase.ebizresults.com CakeBizBase Project
 * @package       App.Test.Fixture
 * @since         CakeBizBase v 1.0
 *
 *
 */
class CompostLocationFixture extends CakeTestFixture
{

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'entity_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'program_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'address_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'name' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'fk_compost_locations_entities1_idx' => array('column' => 'entity_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'entity_id' => 1,
			'program_id' => 1,
			'address_id' => 1,
			'name' => 'Lorem ipsum dolor sit amet'
		),
	);

}
