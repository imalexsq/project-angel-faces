<?php
/**
 * LedgerFixture
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Test.Fixtures
 * @since         PAF v 1.0
 *
 *
 */
class LedgerFixture extends CakeTestFixture
{

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'incometype_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'key' => 'index'),
		'expensetype_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'key' => 'index'),
		'event_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'program_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'key' => 'index'),
		'project_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'key' => 'index'),
		'location_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'key' => 'index'),
		'profile_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'key' => 'index'),
		'amount' => array('type' => 'float', 'null' => true, 'default' => null),
		'name' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 250, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'note' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 250, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'ledger_incometype_id_idx' => array('column' => 'incometype_id', 'unique' => 0),
			'ledger_expensetype_id_idx' => array('column' => 'expensetype_id', 'unique' => 0),
			'ledger_program_id_idx' => array('column' => 'program_id', 'unique' => 0),
			'ledger_project_id_idx' => array('column' => 'project_id', 'unique' => 0),
			'ledger_location_id_idx' => array('column' => 'location_id', 'unique' => 0),
			'ledger_profile_id_idx' => array('column' => 'profile_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'incometype_id' => 1,
			'expensetype_id' => 1,
			'event_id' => 1,
			'program_id' => 1,
			'project_id' => 1,
			'location_id' => 1,
			'profile_id' => 1,
			'amount' => 1,
			'name' => 'Lorem ipsum dolor sit amet',
			'note' => 'Lorem ipsum dolor sit amet',
			'created' => '2013-06-29 15:47:08',
			'modified' => '2013-06-29 15:47:08'
		),
	);

}
