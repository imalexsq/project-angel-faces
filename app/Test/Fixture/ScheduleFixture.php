<?php
/**
 * ScheduleFixture
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Test.Fixtures
 * @since         PAF v 1.0
 *
 *
 */
class ScheduleFixture extends CakeTestFixture
{

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'event_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'functiontype_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'profile_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'scheduletype_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'start' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'end' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'event_id' => 1,
			'functiontype_id' => 1,
			'profile_id' => 1,
			'scheduletype_id' => 1,
			'start' => '2013-06-29 15:47:24',
			'end' => '2013-06-29 15:47:24',
			'created' => '2013-06-29 15:47:24',
			'modified' => '2013-06-29 15:47:24'
		),
	);

}
