<?php
/**
 * TaskFixture
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Test.Fixtures
 * @since         PAF v 1.0
 *
 *
 */
class TaskFixture extends CakeTestFixture
{

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'program_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'project_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'location_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'profile_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'tasktype_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'task' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'task_start' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'task_end' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'complete' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'note' => array('type' => 'text', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'fk_tasks_programs1_idx' => array('column' => 'program_id', 'unique' => 0),
			'fk_tasks_projects1_idx' => array('column' => 'project_id', 'unique' => 0),
			'fk_tasks_locations1_idx' => array('column' => 'location_id', 'unique' => 0),
			'fk_tasks_entities1_idx' => array('column' => 'profile_id', 'unique' => 0),
			'fk_tasks_task_types1_idx' => array('column' => 'tasktype_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'program_id' => 1,
			'project_id' => 1,
			'location_id' => 1,
			'profile_id' => 1,
			'tasktype_id' => 1,
			'task' => 'Lorem ipsum dolor sit amet',
			'task_start' => '2013-06-29 15:47:30',
			'task_end' => '2013-06-29 15:47:30',
			'complete' => 1,
			'note' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'created' => '2013-06-29 15:47:30',
			'modified' => '2013-06-29 15:47:30'
		),
	);

}
