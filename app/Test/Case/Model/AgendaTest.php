<?php
App::uses('Agenda', 'Model');

/**
 * Agenda Test Case
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces

 * @package       App.Test.Model

 * @since         PAF v 1.0
 *
 *
 */
class AgendaTest extends CakeTestCase
{

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.agenda',
		'app.agendatype',
		'app.event',
		'app.location',
		'app.project',
		'app.program',
		'app.document',
		'app.profile',
		'app.profiletype',
		'app.compost',
		'app.yieldtype',
		'app.inventory',
		'app.yield_measure',
		'app.yield',
		'app.ledger',
		'app.incometype',
		'app.expensetype',
		'app.schedule',
		'app.functiontype',
		'app.scheduletype',
		'app.task',
		'app.tasktype',
		'app.address',
		'app.addresstype',
		'app.area',
		'app.addresses_profile',
		'app.phone',
		'app.phones_profile',
		'app.profiles_program',
		'app.team',
		'app.profiles_team',
		'app.projecttype',
		'app.fee'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp()
	{
		parent::setUp();
		$this->Agenda = ClassRegistry::init('Agenda');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown()
	{
		unset($this->Agenda);

		parent::tearDown();
	}

}
