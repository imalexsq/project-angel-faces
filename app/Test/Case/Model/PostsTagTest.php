<?php
App::uses('PostsTag', 'Model');

/**
 * PostsTag Test Case
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces

 * @package       App.Test.Model

 * @since         PAF v 1.0
 *
 *
 */
class PostsTagTest extends CakeTestCase
{

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.posts_tag',
		'app.post',
		'app.comment',
		'app.user',
		'app.group',
		'app.keyword',
		'app.page',
		'app.keywords_page',
		'app.keywords_post',
		'app.tag'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp()
	{
		parent::setUp();
		$this->PostsTag = ClassRegistry::init('PostsTag');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown()
	{
		unset($this->PostsTag);

		parent::tearDown();
	}

}
