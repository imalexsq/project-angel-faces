<?php
App::uses('Incometype', 'Model');

/**
 * Incometype Test Case
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces

 * @package       App.Test.Model

 * @since         PAF v 1.0
 *
 *
 */
class IncometypeTest extends CakeTestCase
{

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.incometype',
		'app.ledger',
		'app.expensetype',
		'app.program',
		'app.document',
		'app.location',
		'app.project',
		'app.projecttype',
		'app.event',
		'app.profile',
		'app.profiletype',
		'app.area',
		'app.compost',
		'app.yieldtype',
		'app.inventory',
		'app.yield_measure',
		'app.yield',
		'app.task',
		'app.tasktype',
		'app.address',
		'app.addresstype',
		'app.areas',
		'app.addresses_profile',
		'app.events_profile',
		'app.phone',
		'app.phones_profile',
		'app.fee'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp()
	{
		parent::setUp();
		$this->Incometype = ClassRegistry::init('Incometype');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown()
	{
		unset($this->Incometype);

		parent::tearDown();
	}

}
