<?php
App::uses('Tasktype', 'Model');

/**
 * Tasktype Test Case
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces

 * @package       App.Test.Model

 * @since         PAF v 1.0
 *
 *
 */
class TasktypeTest extends CakeTestCase
{

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.tasktype',
		'app.task',
		'app.program',
		'app.document',
		'app.location',
		'app.project',
		'app.projecttype',
		'app.event',
		'app.agenda',
		'app.ledger',
		'app.incometype',
		'app.expensetype',
		'app.profile',
		'app.profiletype',
		'app.compost',
		'app.yieldtype',
		'app.inventory',
		'app.yield_measure',
		'app.yield',
		'app.schedule',
		'app.functiontype',
		'app.scheduletype',
		'app.address',
		'app.addresstype',
		'app.area',
		'app.addresses_profile',
		'app.phone',
		'app.phones_profile',
		'app.profiles_program',
		'app.team',
		'app.profiles_team',
		'app.fee'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp()
	{
		parent::setUp();
		$this->Tasktype = ClassRegistry::init('Tasktype');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown()
	{
		unset($this->Tasktype);

		parent::tearDown();
	}

}
