<?php
App::uses('Yieldtype', 'Model');

/**
 * Yieldtype Test Case
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces

 * @package       App.Test.Model

 * @since         PAF v 1.0
 *
 *
 */
class YieldtypeTest extends CakeTestCase
{

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.yieldtype',
		'app.compost',
		'app.profile',
		'app.profiletype',
		'app.document',
		'app.location',
		'app.project',
		'app.program',
		'app.event',
		'app.agenda',
		'app.ledger',
		'app.incometype',
		'app.expensetype',
		'app.schedule',
		'app.functiontype',
		'app.scheduletype',
		'app.task',
		'app.tasktype',
		'app.profiles_program',
		'app.projecttype',
		'app.address',
		'app.addresstype',
		'app.area',
		'app.addresses_profile',
		'app.fee',
		'app.inventory',
		'app.yield_measure',
		'app.yield',
		'app.phone',
		'app.phones_profile',
		'app.team',
		'app.profiles_team'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp()
	{
		parent::setUp();
		$this->Yieldtype = ClassRegistry::init('Yieldtype');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown()
	{
		unset($this->Yieldtype);

		parent::tearDown();
	}

}
