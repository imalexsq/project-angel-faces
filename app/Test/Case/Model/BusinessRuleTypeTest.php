<?php
App::uses('BusinessRuleType', 'Model');

/**
 * BusinessRuleType Test Case
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces

 * @package       App.Test.Model

 * @since         PAF v 1.0
 *
 *
 */
class BusinessRuleTypeTest extends CakeTestCase
{

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.business_rule_type'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp()
	{
		parent::setUp();
		$this->BusinessRuleType = ClassRegistry::init('BusinessRuleType');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown()
	{
		unset($this->BusinessRuleType);

		parent::tearDown();
	}

}
