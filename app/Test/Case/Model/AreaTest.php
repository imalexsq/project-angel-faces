<?php
App::uses('Area', 'Model');

/**
 * Area Test Case
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces

 * @package       App.Test.Model

 * @since         PAF v 1.0
 *
 *
 */
class AreaTest extends CakeTestCase
{

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.area',
		'app.profile',
		'app.profiletype',
		'app.compost',
		'app.location',
		'app.project',
		'app.program',
		'app.document',
		'app.event',
		'app.events_profile',
		'app.ledger',
		'app.incometype',
		'app.expensetype',
		'app.task',
		'app.tasktype',
		'app.projecttype',
		'app.address',
		'app.addresstype',
		'app.areas',
		'app.addresses_profile',
		'app.fee',
		'app.inventory',
		'app.yieldtype',
		'app.yield',
		'app.yield_measure',
		'app.phone',
		'app.phones_profile'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp()
	{
		parent::setUp();
		$this->Area = ClassRegistry::init('Area');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown()
	{
		unset($this->Area);

		parent::tearDown();
	}

}
