<?php
App::uses('TeamsController', 'Controller');

/**
 * TeamsController Test Case
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces

 * @package       App.Test.Controller

 * @since         PAF v 1.0
 *
 *
 */
class TeamsControllerTest extends ControllerTestCase
{

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.team',
		'app.teamtype',
		'app.profile',
		'app.profiletype',
		'app.compost',
		'app.location',
		'app.project',
		'app.program',
		'app.document',
		'app.event',
		'app.agenda',
		'app.agendatype',
		'app.ledger',
		'app.incometype',
		'app.expensetype',
		'app.schedule',
		'app.functiontype',
		'app.scheduletype',
		'app.task',
		'app.tasktype',
		'app.profiles_program',
		'app.projecttype',
		'app.address',
		'app.addresstype',
		'app.area',
		'app.addresses_profile',
		'app.fee',
		'app.inventory',
		'app.yieldtype',
		'app.yield',
		'app.yield_measure',
		'app.phone',
		'app.phones_profile',
		'app.profiles_team'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex()
	{
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView()
	{
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd()
	{
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit()
	{
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete()
	{
	}

/**
 * testAdminIndex method
 *
 * @return void
 */
	public function testAdminIndex()
	{
	}

/**
 * testAdminView method
 *
 * @return void
 */
	public function testAdminView()
	{
	}

/**
 * testAdminAdd method
 *
 * @return void
 */
	public function testAdminAdd()
	{
	}

/**
 * testAdminEdit method
 *
 * @return void
 */
	public function testAdminEdit()
	{
	}

/**
 * testAdminDelete method
 *
 * @return void
 */
	public function testAdminDelete()
	{
	}

}
