<?php
/**
 * Edit Page View
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Views
 * @since         PAF v 1.0
 *
 *
 */
?><div class="matter">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="widget">
                    <div class="widget-head">
                        <div class="pull-left">
                            <h2><?php echo __('Edit Page'); ?></h2>
                        </div>
                        <div class="widget-icons pull-right">
                            <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>
                            <a href="#" class="wclose"><i class="icon-remove"></i></a>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                    <div class="widget-content">
                        <div class="padd">

                            <?php echo $this->Form->create('Page', array('inputDefaults' => array('label' => false), 'class' => 'form form-horizontal')); ?>
                        		<?php echo $this->Form->input('id', array('class' => 'span12')); ?>
                                <fieldset>
                                    <div class="row-fluid">
                                        <div class="span8">
                                            <div class="control-group">
                                                <?php echo $this->Form->label('title', 'Title', array('class' => 'control-label'));?>
                                                <div class="controls">
                                                    <?php echo $this->Form->input('title', array('class' => 'span12')); ?>
                                                </div><!-- .controls -->
                                            </div><!-- .control-group -->

                                            <div class="control-group">
                                                <?php echo $this->Form->label('content', 'Content', array('class' => 'control-label'));?>
                                                <div class="controls">
                                                    <?php echo $this->Form->input('content', array('class' => 'span12')); ?>
                                                </div><!-- .controls -->
                                            </div><!-- .control-group -->

                                        </div>
                                        <div class="span3">
                                            <?php echo $this->Form->label('keyword', 'Keywords', array('class' => 'control-label'));?>
                                            <?php echo $this->Form->input('Keyword');?>
                                            <?php echo $this->Form->label('slug', 'Slug', array('class' => 'control-label'));?>
                                            <?php echo $this->Form->input('slug', array('class' => 'span12')); ?>
                                            <?php echo $this->Form->label('description', 'Description', array('class' => 'control-label'));?>
                                            <?php echo $this->Form->input('description', array('class' => 'span12')); ?>

                                        </div>
                                    </div>
                                </fieldset>
                        </div>
                        <div class="widget-foot">
                            <?php echo $this->Form->submit('Submit', array('class' => 'btn btn-large btn-primary')); ?>
                            <!-- Footer goes here -->
                        </div>
                        <?php echo $this->Form->end(); ?>                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

