<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <div class="row">
                <!-- Logo section -->
                <div class="span8">
                    <!-- Logo. -->
                    <div class="logo">
                        <h3><a><?php echo $this->Html->link('Project<span class="bold"> AngelFaces</span>', array('controller' => 'pages', 'action' => 'display','home'),
                                    array (
                                        'escape' => false)
                                ); ?></a></h3>
                    </div>
                    <!-- Logo ends -->
                </div>
                <div class="span4">
                    <!-- Menu button for smallar screens -->
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span>Menu</span>
                    </a>
                    <!-- Site name for smallar screens -->
                    <a href="index.html" class="brand hidden-desktop">Project AngelFaces</a>
                    <!-- Navigation starts -->
                    <div class="nav-collapse collapse">
                    </div>
                    <!-- Search form -->
                    <form class="navbar-search pull-left">
                        <input type="text" class="search-query" placeholder="Search">
                    </form>
                    <!-- Links -->
                    <ul class="nav pull-right">
                        <li class="dropdown pull-right">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <i class="icon-user"></i> Admin <b class="caret"></b>
                            </a>
                            <!-- Dropdown menu -->
                            <ul class="dropdown-menu">

                                <li><?php echo $this->Html->link('<i class="icon-user"></i> Profile', array('controller' => 'users', 'action' => 'view',$authUser['User']['id']),
                                    array (
                                        'escape' => false)
                                ); ?></li>

                                <li>  <li><?php echo $this->Html->link('<i class="icon-cogs"></i> Settings', array('controller' => 'users', 'action' => 'edit/1'),
                                    array (
                                        'escape' => false)); ?></li></li>
                                <li>  <li><?php echo $this->Html->link('<i class="icon-off"></i> Logout', array('controller' => 'users', 'action' => 'logout'),
                                    array (
                                        'escape' => false)); ?></li></li>

                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>