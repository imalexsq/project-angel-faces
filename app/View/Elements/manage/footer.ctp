<!-- Footer starts -->
<footer>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <!-- Copyright info -->
                <p class="copy">
                    Copyright &copy; <?php echo date('Y', time()) ?> | <a href="/"><?php echo Configure::read('Sandbox.admin.name') ?></a>
                </p>
            </div>
        </div>
    </div>
</footer>
<!-- Scroll to top -->
<span class="totop"><a href="#"><i class="icon-chevron-up"></i></a></span>