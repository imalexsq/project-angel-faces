
<?php
/**
 * @var $this View
 */
?>
<!-- Header starts -->
<header>
    <div class="container">
        <div class="row">
            <div class="span12">
                <div class="row">
                    <div class="span9">
                        <!-- Logo. Use class "color" to add color to the text. -->
                        <div class="logo">
                            <h1><a href="/">Project<span class="color bold"> AngelFaces</span></a></h1>
                            <p class="meta">
                                Cultivating hope and compassion
                            </p>
                        </div>
                    </div>
                    <div class="span3">
                        <div class="social">
                            <a><?php echo $this->Html->link('<i class="icon-facebook facebook"></i>', 'http://www.facebook.com/ProjectAngelFaces', array('target'=>'_blank', 'escape' => false));?></a>
                            <a><?php echo $this->Html->link('<i class="icon-twitter twitter"></i>', 'http://twitter.com/ShareAngelFaces', array('target'=>'_blank', 'escape' => false));?></a>

                            <a><?php echo $this->Html->link('<i class="icon-linkedin linkedin"></i>', 'http://www.linkedin.com/profile/view?id=23097757', array('target'=>'_blank', 'escape' => false));?></a>
                            
                            <a><?php echo $this->Html->link('<i class="icon-google-plus google-plus"></i>', 'http://plus.google.com/104009000826730647441', array('target'=>'_blank', 'escape' => false));?></a>

                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="span9 offset3">
                        <!-- Navigation -->
                        <div class="navbar">
                            <div class="navbar-inner">
                                <div class="container">
                                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </a>
                                    <div class="nav-collapse collapse">
                                        <ul class="nav">
                                            <li>
                                                <?php
                                                    echo $this->Html->link(
                                                        'Home',
                                                        array(
                                                            'controller' => 'pages',
                                                            'action'    => 'display',
                                                            'home'
                                                        )
                                                    );
                                                ?>

                                            </li>
                                             <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">About <b class="caret"></b></a>
                                                <ul class="dropdown-menu">

                                            <li>
                                                <?php
                                                    echo $this->Html->link(
                                                        'About Us',
                                                        array(
                                                            'controller' => 'pages',
                                                            'action'    => 'display',
                                                            'about-us'
                                                        )
                                                    );
                                                ?>                                   
                                            </li>
                                            
                                            <li>
                                                <?php
                                                    echo $this->Html->link(
                                                        'About Our Founder',
                                                        array(
                                                            'controller' => 'pages',
                                                            'action'    => 'display',
                                                            'about-our-founder'
                                                        )
                                                    );
                                                ?>                                   
                                            </li>

                                            <li>
                                                <?php
                                                    echo $this->Html->link(
                                                        'About Our Inspiration',
                                                        array(
                                                            'controller' => 'pages',
                                                            'action'    => 'display',
                                                            'our-inspiration'
                                                        )
                                                    );
                                                ?>                                   
                                            </li>

                                            </ul></li>


                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Programs <b class="caret"></b></a>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <?php
                                                            echo $this->Html->link(
                                                                'Neighborhood Fruit Harvest',
                                                                array(
                                                                    'controller' => 'pages',
                                                                    'action'    => 'display',
                                                                    'neighborhood-fruit-harvest'
                                                                )
                                                            );
                                                        ?>
                                                    </li>
                                                    <li>
                                                        <?php
                                                            echo $this->Html->link(
                                                                'Sustainability Services',
                                                                array(
                                                                    'controller' => 'pages',
                                                                    'action'    => 'display',
                                                                    'sustainability-services'
                                                                )
                                                            );
                                                        ?>
                                                    </li>
                                                    <li>
                                                        <?php
                                                        echo $this->Html->link(
                                                            'Community Support Programs',
                                                            array(
                                                                'controller' => 'pages',
                                                                'action'    => 'display',
                                                                'community-support-programs'
                                                            )
                                                        );
                                                        ?>
                                                    </li>
                                                    <li>
                                                        <?php
                                                            echo $this->Html->link(
                                                                'Urban Tribal Gardens',
                                                                array(
                                                                    'controller' => 'pages',
                                                                    'action'    => 'display',
                                                                    'urban-tribal-gardens'
                                                                )
                                                            );
                                                        ?>
                                                    </li>
                                                </ul>
                                            </li>

                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Get Involved <b class="caret"></b></a>
                                                <ul class="dropdown-menu">
                                                    
                                                    <li>
                                                        <?php
                                                            echo $this->Html->link(
                                                                'Get Involved ',
                                                                array(
                                                                    'controller' => 'pages',
                                                                    'action'    => 'display',
                                                                    'get-involved'
                                                                )
                                                            );
                                                        ?>
                                                    </li>

                                                    <li>
                                                        <?php
                                                            echo $this->Html->link(
                                                                'Wish List',
                                                                array(
                                                                    'controller' => 'pages',
                                                                    'action'    => 'display',
                                                                    'wish-list'
                                                                )
                                                            );
                                                        ?>
                                                    </li>


                                                    <li>
                                                        <?php
                                                            echo $this->Html->link(
                                                                'I want to Donate',
                                                                array(
                                                                    'controller' => 'pages',
                                                                    'action'    => 'display',
                                                                    'donate'
                                                                )
                                                            );
                                                        ?>
                                                    </li>
                                                    <li class="dropdown">

                                                        <a tabindex="-1" href="#">Volunteer at<b class="caret"></b></a>                                                        
                                                        <ul class="dropdown-submenu">
                                                        <li>                                                  
                                                

                                                <?php
                                                            echo $this->Html->link(
                                                                'Whitney Recreation and Senior Center',
                                                                array(
                                                                    'controller' => 'pages',
                                                                    'action'    => 'display',
                                                                    'whitney-recreation-and-senior-center'
                                                                )
                                                            );
                                                        ?>
                                                 </li>       

                                                 <li>
                                                         <?php
                                                            echo $this->Html->link(
                                                                'CSN Cheyenne Campus',
                                                                array(
                                                                    'controller' => 'pages',
                                                                    'action'    => 'display',
                                                                    'csn-cheyenne-campus'
                                                                )
                                                            );
                                                        ?> 
                                                 </li>

                                                 <li>
                                                         <?php
                                                            echo $this->Html->link(
                                                                'UNLV Campus Community',
                                                                array(
                                                                    'controller' => 'pages',
                                                                    'action'    => 'display',
                                                                    'unlv-campus-community'
                                                                )
                                                            );
                                                        ?> 
                                                 </li>

                                                 <li>
                                                         <?php
                                                            echo $this->Html->link(
                                                                'Old Henderson at Jesus Triumphant Christian Complex',
                                                                array(
                                                                    'controller' => 'pages',
                                                                    'action'    => 'display',
                                                                    'old-henderson-at-jesus-triumphant-christian-complex'
                                                                )
                                                            );
                                                        ?> 
                                                 </li>


                                                </ul>
                                                </li>

                                                    
                                                    <li>
                                                        <?php
                                                            if(!$authUser)
                                                            {
                                                                echo $this->Html->link(
                                                                    'Login',
                                                                    array(
                                                                        'controller' => 'users',
                                                                        'action'    => 'login'
                                                                    )
                                                                );
                                                            }
                                                            else
                                                            {
                                                                echo $this->Html->link(
                                                                    'Logout',
                                                                    array(
                                                                        'controller' => 'users',
                                                                        'action'    => 'logout'
                                                                    )
                                                                );

                                                            }
                                                        ?>
                                                    </li>
                                                    
                                                </ul>
                                            </li>
                                            <li>
                                                <?php
                                                    echo $this->Html->link(
                                                        'Blog',
                                                        array(
                                                            'controller' => 'posts',
                                                            'action'    => 'listindex'
                                                        )
                                                    );
                                                ?>
                                            </li>
                                            <li>
                                                <a href="#footer">Contact Us</a>
                                            </li>
                                            <li id="donate">
                                                <?php
                                                    echo $this->Html->link(
                                                        'I want to Donate',
                                                        array(
                                                            'controller' => 'pages',
                                                            'action'    => 'display',
                                                            'donate'
                                                        )
                                                    );
                                                ?>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</header>
<!-- Seperator -->
<div class="sep">
</div>
<!-- Header ends -->