<?php
/**
 * Admin View Profile View
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Views
 * @since         PAF v 1.0
 *
 *
 */
?>
<div class="matter">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="widget">
                    <div class="widget-head">
                        <div class="pull-left">
                            <h2>
                                <?php echo __('Profiles'); ?><?php echo $this->Html->link('Add <i class="icon-plus"></i>',array('controller'=> 'profiles','action'    => 'add'),array('escape' => false, 'class' => 'btn btn-primary btn-large offset1'));?>                            </h2>

                        </div>
                        <div class="widget-icons pull-right">
                            <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>
                            <a href="#" class="wclose"><i class="icon-remove"></i></a>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                    <div class="widget-content">
                        <div class="padd">

        <table class="table table-striped table-bordered">
            <tr>		<td><strong><?php echo __('Id'); ?></strong></td>
		<td>
			<?php echo h($profile['Profile']['id']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Profiletype'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($profile['Profiletype']['id'], array('controller' => 'profiletypes', 'action' => 'view', $profile['Profiletype']['id']), array('class' => '')); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Parent Profile'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($profile['ParentProfile']['id'], array('controller' => 'profiles', 'action' => 'view', $profile['ParentProfile']['id']), array('class' => '')); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('First Name'); ?></strong></td>
		<td>
			<?php echo h($profile['Profile']['first_name']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Last Name'); ?></strong></td>
		<td>
			<?php echo h($profile['Profile']['last_name']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Business Name'); ?></strong></td>
		<td>
			<?php echo h($profile['Profile']['business_name']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Has Car'); ?></strong></td>
		<td>
			<?php echo h($profile['Profile']['has_car']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Created'); ?></strong></td>
		<td>
			<?php echo h($profile['Profile']['created']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Modified'); ?></strong></td>
		<td>
			<?php echo h($profile['Profile']['modified']); ?>
			&nbsp;
		</td>
</tr>        </table>
        <!-- .table table-striped table-bordered -->

    </div>
    <!-- .view -->

    
        <div class="related">

            <h3><?php echo __('Related Composts'); ?></h3>

            <?php if (!empty($profile['Compost'])): ?>

            <table class="table table-striped table-bordered">
                <tr>
                    		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Profile Id'); ?></th>
		<th><?php echo __('Location Id'); ?></th>
		<th><?php echo __('Yieldtype Id'); ?></th>
		<th><?php echo __('Cubic Ft'); ?></th>
		<th><?php echo __('Dt Received'); ?></th>
		<th><?php echo __('Dt Released'); ?></th>
		<th><?php echo __('Price'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
                	<?php
								$i = 0;
								foreach ($profile['Compost'] as $compost): ?>
		<tr>
			<td><?php echo $compost['id']; ?></td>
			<td><?php echo $compost['profile_id']; ?></td>
			<td><?php echo $compost['location_id']; ?></td>
			<td><?php echo $compost['yieldtype_id']; ?></td>
			<td><?php echo $compost['cubic_ft']; ?></td>
			<td><?php echo $compost['dt_received']; ?></td>
			<td><?php echo $compost['dt_released']; ?></td>
			<td><?php echo $compost['price']; ?></td>
			<td><?php echo $compost['created']; ?></td>
			<td><?php echo $compost['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'composts', 'action' => 'view', $compost['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'composts', 'action' => 'edit', $compost['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'composts', 'action' => 'delete', $compost['id']), array('class' => 'btn btn-mini'), __('Are you sure you want to delete # %s?', $compost['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
            </table>
            <!-- .table table-striped table-bordered -->

            <?php endif; ?>


            <div class="actions">
                <?php echo $this->Html->link('<i class="icon-plus icon-white"></i> '.__('New Compost'), array('controller' => 'composts', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>            </div>
            <!-- .actions -->

        </div><!-- .related -->

    
        <div class="related">

            <h3><?php echo __('Related Documents'); ?></h3>

            <?php if (!empty($profile['Document'])): ?>

            <table class="table table-striped table-bordered">
                <tr>
                    		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Location Id'); ?></th>
		<th><?php echo __('Profile Id'); ?></th>
		<th><?php echo __('Program Id'); ?></th>
		<th><?php echo __('Project Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('File'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
                	<?php
								$i = 0;
								foreach ($profile['Document'] as $document): ?>
		<tr>
			<td><?php echo $document['id']; ?></td>
			<td><?php echo $document['location_id']; ?></td>
			<td><?php echo $document['profile_id']; ?></td>
			<td><?php echo $document['program_id']; ?></td>
			<td><?php echo $document['project_id']; ?></td>
			<td><?php echo $document['name']; ?></td>
			<td><?php echo $document['file']; ?></td>
			<td><?php echo $document['created']; ?></td>
			<td><?php echo $document['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'documents', 'action' => 'view', $document['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'documents', 'action' => 'edit', $document['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'documents', 'action' => 'delete', $document['id']), array('class' => 'btn btn-mini'), __('Are you sure you want to delete # %s?', $document['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
            </table>
            <!-- .table table-striped table-bordered -->

            <?php endif; ?>


            <div class="actions">
                <?php echo $this->Html->link('<i class="icon-plus icon-white"></i> '.__('New Document'), array('controller' => 'documents', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>            </div>
            <!-- .actions -->

        </div><!-- .related -->

    
        <div class="related">

            <h3><?php echo __('Related Ledgers'); ?></h3>

            <?php if (!empty($profile['Ledger'])): ?>

            <table class="table table-striped table-bordered">
                <tr>
                    		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Incometype Id'); ?></th>
		<th><?php echo __('Expensetype Id'); ?></th>
		<th><?php echo __('Event Id'); ?></th>
		<th><?php echo __('Program Id'); ?></th>
		<th><?php echo __('Project Id'); ?></th>
		<th><?php echo __('Location Id'); ?></th>
		<th><?php echo __('Profile Id'); ?></th>
		<th><?php echo __('Amount'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Note'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
                	<?php
								$i = 0;
								foreach ($profile['Ledger'] as $ledger): ?>
		<tr>
			<td><?php echo $ledger['id']; ?></td>
			<td><?php echo $ledger['incometype_id']; ?></td>
			<td><?php echo $ledger['expensetype_id']; ?></td>
			<td><?php echo $ledger['event_id']; ?></td>
			<td><?php echo $ledger['program_id']; ?></td>
			<td><?php echo $ledger['project_id']; ?></td>
			<td><?php echo $ledger['location_id']; ?></td>
			<td><?php echo $ledger['profile_id']; ?></td>
			<td><?php echo $ledger['amount']; ?></td>
			<td><?php echo $ledger['name']; ?></td>
			<td><?php echo $ledger['note']; ?></td>
			<td><?php echo $ledger['created']; ?></td>
			<td><?php echo $ledger['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'ledgers', 'action' => 'view', $ledger['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'ledgers', 'action' => 'edit', $ledger['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'ledgers', 'action' => 'delete', $ledger['id']), array('class' => 'btn btn-mini'), __('Are you sure you want to delete # %s?', $ledger['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
            </table>
            <!-- .table table-striped table-bordered -->

            <?php endif; ?>


            <div class="actions">
                <?php echo $this->Html->link('<i class="icon-plus icon-white"></i> '.__('New Ledger'), array('controller' => 'ledgers', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>            </div>
            <!-- .actions -->

        </div><!-- .related -->

    
        <div class="related">

            <h3><?php echo __('Related Locations'); ?></h3>

            <?php if (!empty($profile['Location'])): ?>

            <table class="table table-striped table-bordered">
                <tr>
                    		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Project Id'); ?></th>
		<th><?php echo __('Profile Id'); ?></th>
		<th><?php echo __('Address Id'); ?></th>
		<th><?php echo __('Fee Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Note'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
                	<?php
								$i = 0;
								foreach ($profile['Location'] as $location): ?>
		<tr>
			<td><?php echo $location['id']; ?></td>
			<td><?php echo $location['project_id']; ?></td>
			<td><?php echo $location['profile_id']; ?></td>
			<td><?php echo $location['address_id']; ?></td>
			<td><?php echo $location['fee_id']; ?></td>
			<td><?php echo $location['name']; ?></td>
			<td><?php echo $location['note']; ?></td>
			<td><?php echo $location['created']; ?></td>
			<td><?php echo $location['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'locations', 'action' => 'view', $location['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'locations', 'action' => 'edit', $location['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'locations', 'action' => 'delete', $location['id']), array('class' => 'btn btn-mini'), __('Are you sure you want to delete # %s?', $location['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
            </table>
            <!-- .table table-striped table-bordered -->

            <?php endif; ?>


            <div class="actions">
                <?php echo $this->Html->link('<i class="icon-plus icon-white"></i> '.__('New Location'), array('controller' => 'locations', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>            </div>
            <!-- .actions -->

        </div><!-- .related -->

    
        <div class="related">

            <h3><?php echo __('Related Profiles'); ?></h3>

            <?php if (!empty($profile['ChildProfile'])): ?>

            <table class="table table-striped table-bordered">
                <tr>
                    		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Profiletype Id'); ?></th>
		<th><?php echo __('Parent Id'); ?></th>
		<th><?php echo __('First Name'); ?></th>
		<th><?php echo __('Last Name'); ?></th>
		<th><?php echo __('Business Name'); ?></th>
		<th><?php echo __('Has Car'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
                	<?php
								$i = 0;
								foreach ($profile['ChildProfile'] as $childProfile): ?>
		<tr>
			<td><?php echo $childProfile['id']; ?></td>
			<td><?php echo $childProfile['profiletype_id']; ?></td>
			<td><?php echo $childProfile['parent_id']; ?></td>
			<td><?php echo $childProfile['first_name']; ?></td>
			<td><?php echo $childProfile['last_name']; ?></td>
			<td><?php echo $childProfile['business_name']; ?></td>
			<td><?php echo $childProfile['has_car']; ?></td>
			<td><?php echo $childProfile['created']; ?></td>
			<td><?php echo $childProfile['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'profiles', 'action' => 'view', $childProfile['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'profiles', 'action' => 'edit', $childProfile['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'profiles', 'action' => 'delete', $childProfile['id']), array('class' => 'btn btn-mini'), __('Are you sure you want to delete # %s?', $childProfile['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
            </table>
            <!-- .table table-striped table-bordered -->

            <?php endif; ?>


            <div class="actions">
                <?php echo $this->Html->link('<i class="icon-plus icon-white"></i> '.__('New Child Profile'), array('controller' => 'profiles', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>            </div>
            <!-- .actions -->

        </div><!-- .related -->

    
        <div class="related">

            <h3><?php echo __('Related Schedules'); ?></h3>

            <?php if (!empty($profile['Schedule'])): ?>

            <table class="table table-striped table-bordered">
                <tr>
                    		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Event Id'); ?></th>
		<th><?php echo __('Functiontype Id'); ?></th>
		<th><?php echo __('Profile Id'); ?></th>
		<th><?php echo __('Scheduletype Id'); ?></th>
		<th><?php echo __('Start'); ?></th>
		<th><?php echo __('End'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
                	<?php
								$i = 0;
								foreach ($profile['Schedule'] as $schedule): ?>
		<tr>
			<td><?php echo $schedule['id']; ?></td>
			<td><?php echo $schedule['event_id']; ?></td>
			<td><?php echo $schedule['functiontype_id']; ?></td>
			<td><?php echo $schedule['profile_id']; ?></td>
			<td><?php echo $schedule['scheduletype_id']; ?></td>
			<td><?php echo $schedule['start']; ?></td>
			<td><?php echo $schedule['end']; ?></td>
			<td><?php echo $schedule['created']; ?></td>
			<td><?php echo $schedule['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'schedules', 'action' => 'view', $schedule['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'schedules', 'action' => 'edit', $schedule['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'schedules', 'action' => 'delete', $schedule['id']), array('class' => 'btn btn-mini'), __('Are you sure you want to delete # %s?', $schedule['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
            </table>
            <!-- .table table-striped table-bordered -->

            <?php endif; ?>


            <div class="actions">
                <?php echo $this->Html->link('<i class="icon-plus icon-white"></i> '.__('New Schedule'), array('controller' => 'schedules', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>            </div>
            <!-- .actions -->

        </div><!-- .related -->

    
        <div class="related">

            <h3><?php echo __('Related Tasks'); ?></h3>

            <?php if (!empty($profile['Task'])): ?>

            <table class="table table-striped table-bordered">
                <tr>
                    		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Program Id'); ?></th>
		<th><?php echo __('Project Id'); ?></th>
		<th><?php echo __('Location Id'); ?></th>
		<th><?php echo __('Profile Id'); ?></th>
		<th><?php echo __('Tasktype Id'); ?></th>
		<th><?php echo __('Task'); ?></th>
		<th><?php echo __('Task Start'); ?></th>
		<th><?php echo __('Task End'); ?></th>
		<th><?php echo __('Complete'); ?></th>
		<th><?php echo __('Note'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
                	<?php
								$i = 0;
								foreach ($profile['Task'] as $task): ?>
		<tr>
			<td><?php echo $task['id']; ?></td>
			<td><?php echo $task['program_id']; ?></td>
			<td><?php echo $task['project_id']; ?></td>
			<td><?php echo $task['location_id']; ?></td>
			<td><?php echo $task['profile_id']; ?></td>
			<td><?php echo $task['tasktype_id']; ?></td>
			<td><?php echo $task['task']; ?></td>
			<td><?php echo $task['task_start']; ?></td>
			<td><?php echo $task['task_end']; ?></td>
			<td><?php echo $task['complete']; ?></td>
			<td><?php echo $task['note']; ?></td>
			<td><?php echo $task['created']; ?></td>
			<td><?php echo $task['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'tasks', 'action' => 'view', $task['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'tasks', 'action' => 'edit', $task['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'tasks', 'action' => 'delete', $task['id']), array('class' => 'btn btn-mini'), __('Are you sure you want to delete # %s?', $task['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
            </table>
            <!-- .table table-striped table-bordered -->

            <?php endif; ?>


            <div class="actions">
                <?php echo $this->Html->link('<i class="icon-plus icon-white"></i> '.__('New Task'), array('controller' => 'tasks', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>            </div>
            <!-- .actions -->

        </div><!-- .related -->

    
        <div class="related">

            <h3><?php echo __('Related Addresses'); ?></h3>

            <?php if (!empty($profile['Address'])): ?>

            <table class="table table-striped table-bordered">
                <tr>
                    		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Addresstype Id'); ?></th>
		<th><?php echo __('Area Id'); ?></th>
		<th><?php echo __('Address 1'); ?></th>
		<th><?php echo __('Address 2'); ?></th>
		<th><?php echo __('Address 3'); ?></th>
		<th><?php echo __('City'); ?></th>
		<th><?php echo __('State'); ?></th>
		<th><?php echo __('Zip Code'); ?></th>
		<th><?php echo __('Note'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
                	<?php
								$i = 0;
								foreach ($profile['Address'] as $address): ?>
		<tr>
			<td><?php echo $address['id']; ?></td>
			<td><?php echo $address['addresstype_id']; ?></td>
			<td><?php echo $address['area_id']; ?></td>
			<td><?php echo $address['address_1']; ?></td>
			<td><?php echo $address['address_2']; ?></td>
			<td><?php echo $address['address_3']; ?></td>
			<td><?php echo $address['city']; ?></td>
			<td><?php echo $address['state']; ?></td>
			<td><?php echo $address['zip_code']; ?></td>
			<td><?php echo $address['note']; ?></td>
			<td><?php echo $address['created']; ?></td>
			<td><?php echo $address['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'addresses', 'action' => 'view', $address['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'addresses', 'action' => 'edit', $address['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'addresses', 'action' => 'delete', $address['id']), array('class' => 'btn btn-mini'), __('Are you sure you want to delete # %s?', $address['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
            </table>
            <!-- .table table-striped table-bordered -->

            <?php endif; ?>


            <div class="actions">
                <?php echo $this->Html->link('<i class="icon-plus icon-white"></i> '.__('New Address'), array('controller' => 'addresses', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>            </div>
            <!-- .actions -->

        </div><!-- .related -->

    
        <div class="related">

            <h3><?php echo __('Related Phones'); ?></h3>

            <?php if (!empty($profile['Phone'])): ?>

            <table class="table table-striped table-bordered">
                <tr>
                    		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Phone Type'); ?></th>
		<th><?php echo __('Phone Number'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
                	<?php
								$i = 0;
								foreach ($profile['Phone'] as $phone): ?>
		<tr>
			<td><?php echo $phone['id']; ?></td>
			<td><?php echo $phone['phone_type']; ?></td>
			<td><?php echo $phone['phone_number']; ?></td>
			<td><?php echo $phone['created']; ?></td>
			<td><?php echo $phone['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'phones', 'action' => 'view', $phone['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'phones', 'action' => 'edit', $phone['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'phones', 'action' => 'delete', $phone['id']), array('class' => 'btn btn-mini'), __('Are you sure you want to delete # %s?', $phone['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
            </table>
            <!-- .table table-striped table-bordered -->

            <?php endif; ?>


            <div class="actions">
                <?php echo $this->Html->link('<i class="icon-plus icon-white"></i> '.__('New Phone'), array('controller' => 'phones', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>            </div>
            <!-- .actions -->

        </div><!-- .related -->

    
        <div class="related">

            <h3><?php echo __('Related Programs'); ?></h3>

            <?php if (!empty($profile['Program'])): ?>

            <table class="table table-striped table-bordered">
                <tr>
                    		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Description'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
                	<?php
								$i = 0;
								foreach ($profile['Program'] as $program): ?>
		<tr>
			<td><?php echo $program['id']; ?></td>
			<td><?php echo $program['name']; ?></td>
			<td><?php echo $program['description']; ?></td>
			<td><?php echo $program['created']; ?></td>
			<td><?php echo $program['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'programs', 'action' => 'view', $program['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'programs', 'action' => 'edit', $program['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'programs', 'action' => 'delete', $program['id']), array('class' => 'btn btn-mini'), __('Are you sure you want to delete # %s?', $program['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
            </table>
            <!-- .table table-striped table-bordered -->

            <?php endif; ?>


            <div class="actions">
                <?php echo $this->Html->link('<i class="icon-plus icon-white"></i> '.__('New Program'), array('controller' => 'programs', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>            </div>
            <!-- .actions -->

        </div><!-- .related -->

    
        <div class="related">

            <h3><?php echo __('Related Teams'); ?></h3>

            <?php if (!empty($profile['Team'])): ?>

            <table class="table table-striped table-bordered">
                <tr>
                    		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Teamtype Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Description'); ?></th>
                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
                	<?php
								$i = 0;
								foreach ($profile['Team'] as $team): ?>
		<tr>
			<td><?php echo $team['id']; ?></td>
			<td><?php echo $team['teamtype_id']; ?></td>
			<td><?php echo $team['name']; ?></td>
			<td><?php echo $team['description']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'teams', 'action' => 'view', $team['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'teams', 'action' => 'edit', $team['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'teams', 'action' => 'delete', $team['id']), array('class' => 'btn btn-mini'), __('Are you sure you want to delete # %s?', $team['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
            </table>
            <!-- .table table-striped table-bordered -->

            <?php endif; ?>


            <div class="actions">
                <?php echo $this->Html->link('<i class="icon-plus icon-white"></i> '.__('New Team'), array('controller' => 'teams', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>            </div>
            <!-- .actions -->

        </div><!-- .related -->

                        </div>
                    <div class="widget-foot">
                        <!-- Footer goes here -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
