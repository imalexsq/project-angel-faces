<?php
/**
 * Admin Edit Compost View
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Views
 * @since         PAF v 1.0
 *
 *
 */
?><div class="matter">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="widget">
                    <div class="widget-head">
                        <div class="pull-left">
                            <h2><?php echo __('Admin Edit Compost'); ?></h2>
                        </div>
                        <div class="widget-icons pull-right">
                            <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>
                            <a href="#" class="wclose"><i class="icon-remove"></i></a>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                    <div class="widget-content">
                        <div class="padd">

                            <?php echo $this->Form->create('Compost', array('inputDefaults' => array('label' => false), 'class' => 'form form-horizontal')); ?>
                            <fieldset>

                                <div class="control-group">
	<?php echo $this->Form->label('id', 'Id', array('class' => 'control-label'));?>
	<div class="controls">
		<?php echo $this->Form->input('id', array('class' => 'span12')); ?>
	</div><!-- .controls -->
</div><!-- .control-group -->

<div class="control-group">
	<?php echo $this->Form->label('profile_id', 'Profile Id', array('class' => 'control-label'));?>
	<div class="controls">
		<?php echo $this->Form->input('profile_id', array('class' => 'span12')); ?>
	</div><!-- .controls -->
</div><!-- .control-group -->

<div class="control-group">
	<?php echo $this->Form->label('location_id', 'Location Id', array('class' => 'control-label'));?>
	<div class="controls">
		<?php echo $this->Form->input('location_id', array('class' => 'span12')); ?>
	</div><!-- .controls -->
</div><!-- .control-group -->

<div class="control-group">
	<?php echo $this->Form->label('yieldtype_id', 'Yieldtype Id', array('class' => 'control-label'));?>
	<div class="controls">
		<?php echo $this->Form->input('yieldtype_id', array('class' => 'span12')); ?>
	</div><!-- .controls -->
</div><!-- .control-group -->

<div class="control-group">
	<?php echo $this->Form->label('cubic_ft', 'Cubic Ft', array('class' => 'control-label'));?>
	<div class="controls">
		<?php echo $this->Form->input('cubic_ft', array('class' => 'span12')); ?>
	</div><!-- .controls -->
</div><!-- .control-group -->

<div class="control-group">
	<?php echo $this->Form->label('dt_received', 'Dt Received', array('class' => 'control-label'));?>
	<div class="controls">
		<?php echo $this->Form->input('dt_received', array('class' => 'span12')); ?>
	</div><!-- .controls -->
</div><!-- .control-group -->

<div class="control-group">
	<?php echo $this->Form->label('dt_released', 'Dt Released', array('class' => 'control-label'));?>
	<div class="controls">
		<?php echo $this->Form->input('dt_released', array('class' => 'span12')); ?>
	</div><!-- .controls -->
</div><!-- .control-group -->

<div class="control-group">
	<?php echo $this->Form->label('price', 'Price', array('class' => 'control-label'));?>
	<div class="controls">
		<?php echo $this->Form->input('price', array('class' => 'span12')); ?>
	</div><!-- .controls -->
</div><!-- .control-group -->

                            </fieldset>
                            <?php echo $this->Form->submit('Submit', array('class' => 'btn btn-large btn-primary')); ?>
<?php echo $this->Form->end(); ?>
                        </div>
                        <div class="widget-foot">
                            <!-- Footer goes here -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

