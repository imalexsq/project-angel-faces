<?php
/**
 * Admin View Compost View
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Views
 * @since         PAF v 1.0
 *
 *
 */
?>
<div class="matter">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="widget">
                    <div class="widget-head">
                        <div class="pull-left">
                            <h2>
                                <?php echo __('Composts'); ?><?php echo $this->Html->link('Add <i class="icon-plus"></i>',array('controller'=> 'composts','action'    => 'add'),array('escape' => false, 'class' => 'btn btn-primary btn-large offset1'));?>                            </h2>

                        </div>
                        <div class="widget-icons pull-right">
                            <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>
                            <a href="#" class="wclose"><i class="icon-remove"></i></a>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                    <div class="widget-content">
                        <div class="padd">

        <table class="table table-striped table-bordered">
            <tr>		<td><strong><?php echo __('Id'); ?></strong></td>
		<td>
			<?php echo h($compost['Compost']['id']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Profile'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($compost['Profile']['id'], array('controller' => 'profiles', 'action' => 'view', $compost['Profile']['id']), array('class' => '')); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Location'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($compost['Location']['name'], array('controller' => 'locations', 'action' => 'view', $compost['Location']['id']), array('class' => '')); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Yieldtype'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($compost['Yieldtype']['id'], array('controller' => 'yieldtypes', 'action' => 'view', $compost['Yieldtype']['id']), array('class' => '')); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Cubic Ft'); ?></strong></td>
		<td>
			<?php echo h($compost['Compost']['cubic_ft']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Dt Received'); ?></strong></td>
		<td>
			<?php echo h($compost['Compost']['dt_received']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Dt Released'); ?></strong></td>
		<td>
			<?php echo h($compost['Compost']['dt_released']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Price'); ?></strong></td>
		<td>
			<?php echo h($compost['Compost']['price']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Created'); ?></strong></td>
		<td>
			<?php echo h($compost['Compost']['created']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Modified'); ?></strong></td>
		<td>
			<?php echo h($compost['Compost']['modified']); ?>
			&nbsp;
		</td>
</tr>        </table>
        <!-- .table table-striped table-bordered -->

    </div>
    <!-- .view -->

                        </div>
                    <div class="widget-foot">
                        <!-- Footer goes here -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
