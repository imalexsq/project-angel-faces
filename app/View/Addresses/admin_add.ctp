<?php
/**
 * Admin Add Address View
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Views
 * @since         PAF v 1.0
 *
 *
 */
?><div class="matter">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="widget">
                    <div class="widget-head">
                        <div class="pull-left">
                            <h2><?php echo __('Admin Add Address'); ?></h2>
                        </div>
                        <div class="widget-icons pull-right">
                            <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>
                            <a href="#" class="wclose"><i class="icon-remove"></i></a>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                    <div class="widget-content">
                        <div class="padd">

                            <?php echo $this->Form->create('Address', array('inputDefaults' => array('label' => false), 'class' => 'form form-horizontal')); ?>
                            <fieldset>

                                <div class="control-group">
	<?php echo $this->Form->label('addresstype_id', 'Addresstype Id', array('class' => 'control-label'));?>
	<div class="controls">
		<?php echo $this->Form->input('addresstype_id', array('class' => 'span12')); ?>
	</div><!-- .controls -->
</div><!-- .control-group -->

<div class="control-group">
	<?php echo $this->Form->label('area_id', 'Area Id', array('class' => 'control-label'));?>
	<div class="controls">
		<?php echo $this->Form->input('area_id', array('class' => 'span12')); ?>
	</div><!-- .controls -->
</div><!-- .control-group -->

<div class="control-group">
	<?php echo $this->Form->label('address_1', 'Address 1', array('class' => 'control-label'));?>
	<div class="controls">
		<?php echo $this->Form->input('address_1', array('class' => 'span12')); ?>
	</div><!-- .controls -->
</div><!-- .control-group -->

<div class="control-group">
	<?php echo $this->Form->label('address_2', 'Address 2', array('class' => 'control-label'));?>
	<div class="controls">
		<?php echo $this->Form->input('address_2', array('class' => 'span12')); ?>
	</div><!-- .controls -->
</div><!-- .control-group -->

<div class="control-group">
	<?php echo $this->Form->label('address_3', 'Address 3', array('class' => 'control-label'));?>
	<div class="controls">
		<?php echo $this->Form->input('address_3', array('class' => 'span12')); ?>
	</div><!-- .controls -->
</div><!-- .control-group -->

<div class="control-group">
	<?php echo $this->Form->label('city', 'City', array('class' => 'control-label'));?>
	<div class="controls">
		<?php echo $this->Form->input('city', array('class' => 'span12')); ?>
	</div><!-- .controls -->
</div><!-- .control-group -->

<div class="control-group">
	<?php echo $this->Form->label('state', 'State', array('class' => 'control-label'));?>
	<div class="controls">
		<?php echo $this->Form->input('state', array('class' => 'span12')); ?>
	</div><!-- .controls -->
</div><!-- .control-group -->

<div class="control-group">
	<?php echo $this->Form->label('zip_code', 'Zip Code', array('class' => 'control-label'));?>
	<div class="controls">
		<?php echo $this->Form->input('zip_code', array('class' => 'span12')); ?>
	</div><!-- .controls -->
</div><!-- .control-group -->

<div class="control-group">
	<?php echo $this->Form->label('note', 'Note', array('class' => 'control-label'));?>
	<div class="controls">
		<?php echo $this->Form->input('note', array('class' => 'span12')); ?>
	</div><!-- .controls -->
</div><!-- .control-group -->

		<?php echo $this->Form->input('Profile');?>
                            </fieldset>
                            <?php echo $this->Form->submit('Submit', array('class' => 'btn btn-large btn-primary')); ?>
<?php echo $this->Form->end(); ?>
                        </div>
                        <div class="widget-foot">
                            <!-- Footer goes here -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

