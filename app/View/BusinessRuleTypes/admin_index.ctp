<?php
/**
 * Admin Index Business Rule Type View
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Views
 * @since         PAF v 1.0
 *
 *
 */
?>
<div id="page-content" class="span9">

    <div class="businessRuleTypes index">
        <div class="row-fluid">
            <h2>
                <?php echo __('Business Rule Types'); ?><?php echo $this->Html->link('Add <i class="icon-plus"></i>',array('controller'=> 'businessRuleTypes','action'    => 'add'),array('escape' => false, 'class' => 'btn btn-primary btn-large offset1'));?>            </h2>
        </div>

        <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered">
            <tr>
                                    <th><?php echo $this->Paginator->sort('id'); ?></th>
                                    <th><?php echo $this->Paginator->sort('type'); ?></th>
                                <th class="actions"><?php echo __('Actions'); ?></th>
            </tr>
            <?php
				foreach ($businessRuleTypes as $businessRuleType): ?>
	<tr>
		<td><?php echo h($businessRuleType['BusinessRuleType']['id']); ?>&nbsp;</td>
		<td><?php echo h($businessRuleType['BusinessRuleType']['type']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $businessRuleType['BusinessRuleType']['id']), array('class' => 'btn btn-mini')); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $businessRuleType['BusinessRuleType']['id']), array('class' => 'btn btn-mini')); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $businessRuleType['BusinessRuleType']['id']), array('class' => 'btn btn-mini'), __('Are you sure you want to delete # %s?', $businessRuleType['BusinessRuleType']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
        </table>

        <p>
            <small>
                <?php
				echo $this->Paginator->counter(array(
				'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
				));
				?>            </small>
        </p>

        <div class="pagination">
            <ul>
                <?php
		echo $this->Paginator->prev('< ' . __('Previous'), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
		echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
		echo $this->Paginator->next(__('Next') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
	?>
            </ul>
        </div>
        <!-- .pagination -->

    </div>
    <!-- .index -->

</div><!-- #page-content .span9 -->