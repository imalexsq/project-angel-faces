<?php
/**
 * Admin Add Business Rule Type View
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Views
 * @since         PAF v 1.0
 *
 *
 */
?><div id="page-content" class="span9">

    <div class="businessRuleTypes form">

        <?php echo $this->Form->create('BusinessRuleType', array('inputDefaults' => array('label' => false), 'class' => 'form form-horizontal')); ?>
        <fieldset>
            <h2><?php echo __('Admin Add Business Rule Type'); ?></h2>
            <div class="control-group">
	<?php echo $this->Form->label('type', 'Type', array('class' => 'control-label'));?>
	<div class="controls">
		<?php echo $this->Form->input('type', array('class' => 'span12')); ?>
	</div><!-- .controls -->
</div><!-- .control-group -->

        </fieldset>
        <?php echo $this->Form->submit('Submit', array('class' => 'btn btn-large btn-primary')); ?>
<?php echo $this->Form->end(); ?>

    </div>

</div><!-- #page-content .span9 -->

