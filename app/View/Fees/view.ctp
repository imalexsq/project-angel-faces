<?php
/**
 * View Fee View
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Views
 * @since         PAF v 1.0
 *
 *
 */
?>
<div class="matter">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="widget">
                    <div class="widget-head">
                        <div class="pull-left">
                            <h2>
                                <?php echo __('Fees'); ?><?php echo $this->Html->link('Add <i class="icon-plus"></i>',array('controller'=> 'fees','action'    => 'add'),array('escape' => false, 'class' => 'btn btn-primary btn-large offset1'));?>                            </h2>

                        </div>
                        <div class="widget-icons pull-right">
                            <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>
                            <a href="#" class="wclose"><i class="icon-remove"></i></a>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                    <div class="widget-content">
                        <div class="padd">

        <table class="table table-striped table-bordered">
            <tr>		<td><strong><?php echo __('Id'); ?></strong></td>
		<td>
			<?php echo h($fee['Fee']['id']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Recurs'); ?></strong></td>
		<td>
			<?php echo h($fee['Fee']['recurs']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Name'); ?></strong></td>
		<td>
			<?php echo h($fee['Fee']['name']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Price'); ?></strong></td>
		<td>
			<?php echo h($fee['Fee']['price']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Start'); ?></strong></td>
		<td>
			<?php echo h($fee['Fee']['start']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('End'); ?></strong></td>
		<td>
			<?php echo h($fee['Fee']['end']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Created'); ?></strong></td>
		<td>
			<?php echo h($fee['Fee']['created']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Modified'); ?></strong></td>
		<td>
			<?php echo h($fee['Fee']['modified']); ?>
			&nbsp;
		</td>
</tr>        </table>
        <!-- .table table-striped table-bordered -->

    </div>
    <!-- .view -->

    
        <div class="related">

            <h3><?php echo __('Related Locations'); ?></h3>

            <?php if (!empty($fee['Location'])): ?>

            <table class="table table-striped table-bordered">
                <tr>
                    		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Project Id'); ?></th>
		<th><?php echo __('Profile Id'); ?></th>
		<th><?php echo __('Address Id'); ?></th>
		<th><?php echo __('Fee Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Note'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
                	<?php
								$i = 0;
								foreach ($fee['Location'] as $location): ?>
		<tr>
			<td><?php echo $location['id']; ?></td>
			<td><?php echo $location['project_id']; ?></td>
			<td><?php echo $location['profile_id']; ?></td>
			<td><?php echo $location['address_id']; ?></td>
			<td><?php echo $location['fee_id']; ?></td>
			<td><?php echo $location['name']; ?></td>
			<td><?php echo $location['note']; ?></td>
			<td><?php echo $location['created']; ?></td>
			<td><?php echo $location['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'locations', 'action' => 'view', $location['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'locations', 'action' => 'edit', $location['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'locations', 'action' => 'delete', $location['id']), array('class' => 'btn btn-mini'), __('Are you sure you want to delete # %s?', $location['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
            </table>
            <!-- .table table-striped table-bordered -->

            <?php endif; ?>


            <div class="actions">
                <?php echo $this->Html->link('<i class="icon-plus icon-white"></i> '.__('New Location'), array('controller' => 'locations', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>            </div>
            <!-- .actions -->

        </div><!-- .related -->

                        </div>
                    <div class="widget-foot">
                        <!-- Footer goes here -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
