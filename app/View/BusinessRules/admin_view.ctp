<?php
/**
 * Admin View Business Rule View
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Views
 * @since         PAF v 1.0
 *
 *
 */
?>
<div id="page-content" class="span9">

    <div class="businessRules view">
        <div class="row-fluid">
            <h2>
                <?php  echo __('Business Rule'); ?><?php echo $this->Html->link('Add <i class="icon-plus"></i>',array('controller'=> 'businessRules','action'    => 'add'),array('escape' => false, 'class' => 'btn btn-primary btn-large offset1'));?>            </h2>
        </div>

        <table class="table table-striped table-bordered">
            <tr>		<td><strong><?php echo __('Id'); ?></strong></td>
		<td>
			<?php echo h($businessRule['BusinessRule']['id']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Rule'); ?></strong></td>
		<td>
			<?php echo h($businessRule['BusinessRule']['rule']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Module'); ?></strong></td>
		<td>
			<?php echo h($businessRule['BusinessRule']['module']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Note'); ?></strong></td>
		<td>
			<?php echo h($businessRule['BusinessRule']['note']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Created Dt'); ?></strong></td>
		<td>
			<?php echo h($businessRule['BusinessRule']['created_dt']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Removed Dt'); ?></strong></td>
		<td>
			<?php echo h($businessRule['BusinessRule']['removed_dt']); ?>
			&nbsp;
		</td>
</tr>        </table>
        <!-- .table table-striped table-bordered -->

    </div>
    <!-- .view -->

    
</div><!-- #page-content .span9 -->