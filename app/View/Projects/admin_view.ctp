<?php
/**
 * Admin View Project View
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Views
 * @since         PAF v 1.0
 *
 *
 */
?>
<div class="matter">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="widget">
                    <div class="widget-head">
                        <div class="pull-left">
                            <h2>
                                <?php echo __('Projects'); ?><?php echo $this->Html->link('Add <i class="icon-plus"></i>',array('controller'=> 'projects','action'    => 'add'),array('escape' => false, 'class' => 'btn btn-primary btn-large offset1'));?>                            </h2>

                        </div>
                        <div class="widget-icons pull-right">
                            <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>
                            <a href="#" class="wclose"><i class="icon-remove"></i></a>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                    <div class="widget-content">
                        <div class="padd">

        <table class="table table-striped table-bordered">
            <tr>		<td><strong><?php echo __('Id'); ?></strong></td>
		<td>
			<?php echo h($project['Project']['id']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Program'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($project['Program']['name'], array('controller' => 'programs', 'action' => 'view', $project['Program']['id']), array('class' => '')); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Projecttype'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($project['Projecttype']['id'], array('controller' => 'projecttypes', 'action' => 'view', $project['Projecttype']['id']), array('class' => '')); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Name'); ?></strong></td>
		<td>
			<?php echo h($project['Project']['name']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Start'); ?></strong></td>
		<td>
			<?php echo h($project['Project']['start']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('End'); ?></strong></td>
		<td>
			<?php echo h($project['Project']['end']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Note'); ?></strong></td>
		<td>
			<?php echo h($project['Project']['note']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Created'); ?></strong></td>
		<td>
			<?php echo h($project['Project']['created']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Modified'); ?></strong></td>
		<td>
			<?php echo h($project['Project']['modified']); ?>
			&nbsp;
		</td>
</tr>        </table>
        <!-- .table table-striped table-bordered -->

    </div>
    <!-- .view -->

    
        <div class="related">

            <h3><?php echo __('Related Documents'); ?></h3>

            <?php if (!empty($project['Document'])): ?>

            <table class="table table-striped table-bordered">
                <tr>
                    		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Location Id'); ?></th>
		<th><?php echo __('Profile Id'); ?></th>
		<th><?php echo __('Program Id'); ?></th>
		<th><?php echo __('Project Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('File'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
                	<?php
								$i = 0;
								foreach ($project['Document'] as $document): ?>
		<tr>
			<td><?php echo $document['id']; ?></td>
			<td><?php echo $document['location_id']; ?></td>
			<td><?php echo $document['profile_id']; ?></td>
			<td><?php echo $document['program_id']; ?></td>
			<td><?php echo $document['project_id']; ?></td>
			<td><?php echo $document['name']; ?></td>
			<td><?php echo $document['file']; ?></td>
			<td><?php echo $document['created']; ?></td>
			<td><?php echo $document['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'documents', 'action' => 'view', $document['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'documents', 'action' => 'edit', $document['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'documents', 'action' => 'delete', $document['id']), array('class' => 'btn btn-mini'), __('Are you sure you want to delete # %s?', $document['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
            </table>
            <!-- .table table-striped table-bordered -->

            <?php endif; ?>


            <div class="actions">
                <?php echo $this->Html->link('<i class="icon-plus icon-white"></i> '.__('New Document'), array('controller' => 'documents', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>            </div>
            <!-- .actions -->

        </div><!-- .related -->

    
        <div class="related">

            <h3><?php echo __('Related Events'); ?></h3>

            <?php if (!empty($project['Event'])): ?>

            <table class="table table-striped table-bordered">
                <tr>
                    		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Location Id'); ?></th>
		<th><?php echo __('Program Id'); ?></th>
		<th><?php echo __('Project Id'); ?></th>
		<th><?php echo __('Event Name'); ?></th>
		<th><?php echo __('Event Start'); ?></th>
		<th><?php echo __('Event End'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
                	<?php
								$i = 0;
								foreach ($project['Event'] as $event): ?>
		<tr>
			<td><?php echo $event['id']; ?></td>
			<td><?php echo $event['location_id']; ?></td>
			<td><?php echo $event['program_id']; ?></td>
			<td><?php echo $event['project_id']; ?></td>
			<td><?php echo $event['event_name']; ?></td>
			<td><?php echo $event['event_start']; ?></td>
			<td><?php echo $event['event_end']; ?></td>
			<td><?php echo $event['created']; ?></td>
			<td><?php echo $event['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'events', 'action' => 'view', $event['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'events', 'action' => 'edit', $event['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'events', 'action' => 'delete', $event['id']), array('class' => 'btn btn-mini'), __('Are you sure you want to delete # %s?', $event['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
            </table>
            <!-- .table table-striped table-bordered -->

            <?php endif; ?>


            <div class="actions">
                <?php echo $this->Html->link('<i class="icon-plus icon-white"></i> '.__('New Event'), array('controller' => 'events', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>            </div>
            <!-- .actions -->

        </div><!-- .related -->

    
        <div class="related">

            <h3><?php echo __('Related Ledgers'); ?></h3>

            <?php if (!empty($project['Ledger'])): ?>

            <table class="table table-striped table-bordered">
                <tr>
                    		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Incometype Id'); ?></th>
		<th><?php echo __('Expensetype Id'); ?></th>
		<th><?php echo __('Event Id'); ?></th>
		<th><?php echo __('Program Id'); ?></th>
		<th><?php echo __('Project Id'); ?></th>
		<th><?php echo __('Location Id'); ?></th>
		<th><?php echo __('Profile Id'); ?></th>
		<th><?php echo __('Amount'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Note'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
                	<?php
								$i = 0;
								foreach ($project['Ledger'] as $ledger): ?>
		<tr>
			<td><?php echo $ledger['id']; ?></td>
			<td><?php echo $ledger['incometype_id']; ?></td>
			<td><?php echo $ledger['expensetype_id']; ?></td>
			<td><?php echo $ledger['event_id']; ?></td>
			<td><?php echo $ledger['program_id']; ?></td>
			<td><?php echo $ledger['project_id']; ?></td>
			<td><?php echo $ledger['location_id']; ?></td>
			<td><?php echo $ledger['profile_id']; ?></td>
			<td><?php echo $ledger['amount']; ?></td>
			<td><?php echo $ledger['name']; ?></td>
			<td><?php echo $ledger['note']; ?></td>
			<td><?php echo $ledger['created']; ?></td>
			<td><?php echo $ledger['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'ledgers', 'action' => 'view', $ledger['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'ledgers', 'action' => 'edit', $ledger['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'ledgers', 'action' => 'delete', $ledger['id']), array('class' => 'btn btn-mini'), __('Are you sure you want to delete # %s?', $ledger['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
            </table>
            <!-- .table table-striped table-bordered -->

            <?php endif; ?>


            <div class="actions">
                <?php echo $this->Html->link('<i class="icon-plus icon-white"></i> '.__('New Ledger'), array('controller' => 'ledgers', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>            </div>
            <!-- .actions -->

        </div><!-- .related -->

    
        <div class="related">

            <h3><?php echo __('Related Locations'); ?></h3>

            <?php if (!empty($project['Location'])): ?>

            <table class="table table-striped table-bordered">
                <tr>
                    		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Project Id'); ?></th>
		<th><?php echo __('Profile Id'); ?></th>
		<th><?php echo __('Address Id'); ?></th>
		<th><?php echo __('Fee Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Note'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
                	<?php
								$i = 0;
								foreach ($project['Location'] as $location): ?>
		<tr>
			<td><?php echo $location['id']; ?></td>
			<td><?php echo $location['project_id']; ?></td>
			<td><?php echo $location['profile_id']; ?></td>
			<td><?php echo $location['address_id']; ?></td>
			<td><?php echo $location['fee_id']; ?></td>
			<td><?php echo $location['name']; ?></td>
			<td><?php echo $location['note']; ?></td>
			<td><?php echo $location['created']; ?></td>
			<td><?php echo $location['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'locations', 'action' => 'view', $location['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'locations', 'action' => 'edit', $location['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'locations', 'action' => 'delete', $location['id']), array('class' => 'btn btn-mini'), __('Are you sure you want to delete # %s?', $location['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
            </table>
            <!-- .table table-striped table-bordered -->

            <?php endif; ?>


            <div class="actions">
                <?php echo $this->Html->link('<i class="icon-plus icon-white"></i> '.__('New Location'), array('controller' => 'locations', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>            </div>
            <!-- .actions -->

        </div><!-- .related -->

    
        <div class="related">

            <h3><?php echo __('Related Tasks'); ?></h3>

            <?php if (!empty($project['Task'])): ?>

            <table class="table table-striped table-bordered">
                <tr>
                    		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Program Id'); ?></th>
		<th><?php echo __('Project Id'); ?></th>
		<th><?php echo __('Location Id'); ?></th>
		<th><?php echo __('Profile Id'); ?></th>
		<th><?php echo __('Tasktype Id'); ?></th>
		<th><?php echo __('Task'); ?></th>
		<th><?php echo __('Task Start'); ?></th>
		<th><?php echo __('Task End'); ?></th>
		<th><?php echo __('Complete'); ?></th>
		<th><?php echo __('Note'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
                	<?php
								$i = 0;
								foreach ($project['Task'] as $task): ?>
		<tr>
			<td><?php echo $task['id']; ?></td>
			<td><?php echo $task['program_id']; ?></td>
			<td><?php echo $task['project_id']; ?></td>
			<td><?php echo $task['location_id']; ?></td>
			<td><?php echo $task['profile_id']; ?></td>
			<td><?php echo $task['tasktype_id']; ?></td>
			<td><?php echo $task['task']; ?></td>
			<td><?php echo $task['task_start']; ?></td>
			<td><?php echo $task['task_end']; ?></td>
			<td><?php echo $task['complete']; ?></td>
			<td><?php echo $task['note']; ?></td>
			<td><?php echo $task['created']; ?></td>
			<td><?php echo $task['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'tasks', 'action' => 'view', $task['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'tasks', 'action' => 'edit', $task['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'tasks', 'action' => 'delete', $task['id']), array('class' => 'btn btn-mini'), __('Are you sure you want to delete # %s?', $task['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
            </table>
            <!-- .table table-striped table-bordered -->

            <?php endif; ?>


            <div class="actions">
                <?php echo $this->Html->link('<i class="icon-plus icon-white"></i> '.__('New Task'), array('controller' => 'tasks', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>            </div>
            <!-- .actions -->

        </div><!-- .related -->

                        </div>
                    <div class="widget-foot">
                        <!-- Footer goes here -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
