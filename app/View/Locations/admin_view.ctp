<?php
/**
 * Admin View Location View
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Views
 * @since         PAF v 1.0
 *
 *
 */
?>
<div class="matter">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="widget">
                    <div class="widget-head">
                        <div class="pull-left">
                            <h2>
                                <?php echo __('Locations'); ?><?php echo $this->Html->link('Add <i class="icon-plus"></i>',array('controller'=> 'locations','action'    => 'add'),array('escape' => false, 'class' => 'btn btn-primary btn-large offset1'));?>                            </h2>

                        </div>
                        <div class="widget-icons pull-right">
                            <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>
                            <a href="#" class="wclose"><i class="icon-remove"></i></a>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                    <div class="widget-content">
                        <div class="padd">

        <table class="table table-striped table-bordered">
            <tr>		<td><strong><?php echo __('Id'); ?></strong></td>
		<td>
			<?php echo h($location['Location']['id']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Project'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($location['Project']['name'], array('controller' => 'projects', 'action' => 'view', $location['Project']['id']), array('class' => '')); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Profile'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($location['Profile']['id'], array('controller' => 'profiles', 'action' => 'view', $location['Profile']['id']), array('class' => '')); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Address'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($location['Address']['id'], array('controller' => 'addresses', 'action' => 'view', $location['Address']['id']), array('class' => '')); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Fee'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($location['Fee']['name'], array('controller' => 'fees', 'action' => 'view', $location['Fee']['id']), array('class' => '')); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Name'); ?></strong></td>
		<td>
			<?php echo h($location['Location']['name']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Note'); ?></strong></td>
		<td>
			<?php echo h($location['Location']['note']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Created'); ?></strong></td>
		<td>
			<?php echo h($location['Location']['created']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Modified'); ?></strong></td>
		<td>
			<?php echo h($location['Location']['modified']); ?>
			&nbsp;
		</td>
</tr>        </table>
        <!-- .table table-striped table-bordered -->

    </div>
    <!-- .view -->

    
        <div class="related">

            <h3><?php echo __('Related Composts'); ?></h3>

            <?php if (!empty($location['Compost'])): ?>

            <table class="table table-striped table-bordered">
                <tr>
                    		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Profile Id'); ?></th>
		<th><?php echo __('Location Id'); ?></th>
		<th><?php echo __('Yieldtype Id'); ?></th>
		<th><?php echo __('Cubic Ft'); ?></th>
		<th><?php echo __('Dt Received'); ?></th>
		<th><?php echo __('Dt Released'); ?></th>
		<th><?php echo __('Price'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
                	<?php
								$i = 0;
								foreach ($location['Compost'] as $compost): ?>
		<tr>
			<td><?php echo $compost['id']; ?></td>
			<td><?php echo $compost['profile_id']; ?></td>
			<td><?php echo $compost['location_id']; ?></td>
			<td><?php echo $compost['yieldtype_id']; ?></td>
			<td><?php echo $compost['cubic_ft']; ?></td>
			<td><?php echo $compost['dt_received']; ?></td>
			<td><?php echo $compost['dt_released']; ?></td>
			<td><?php echo $compost['price']; ?></td>
			<td><?php echo $compost['created']; ?></td>
			<td><?php echo $compost['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'composts', 'action' => 'view', $compost['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'composts', 'action' => 'edit', $compost['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'composts', 'action' => 'delete', $compost['id']), array('class' => 'btn btn-mini'), __('Are you sure you want to delete # %s?', $compost['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
            </table>
            <!-- .table table-striped table-bordered -->

            <?php endif; ?>


            <div class="actions">
                <?php echo $this->Html->link('<i class="icon-plus icon-white"></i> '.__('New Compost'), array('controller' => 'composts', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>            </div>
            <!-- .actions -->

        </div><!-- .related -->

    
        <div class="related">

            <h3><?php echo __('Related Documents'); ?></h3>

            <?php if (!empty($location['Document'])): ?>

            <table class="table table-striped table-bordered">
                <tr>
                    		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Location Id'); ?></th>
		<th><?php echo __('Profile Id'); ?></th>
		<th><?php echo __('Program Id'); ?></th>
		<th><?php echo __('Project Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('File'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
                	<?php
								$i = 0;
								foreach ($location['Document'] as $document): ?>
		<tr>
			<td><?php echo $document['id']; ?></td>
			<td><?php echo $document['location_id']; ?></td>
			<td><?php echo $document['profile_id']; ?></td>
			<td><?php echo $document['program_id']; ?></td>
			<td><?php echo $document['project_id']; ?></td>
			<td><?php echo $document['name']; ?></td>
			<td><?php echo $document['file']; ?></td>
			<td><?php echo $document['created']; ?></td>
			<td><?php echo $document['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'documents', 'action' => 'view', $document['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'documents', 'action' => 'edit', $document['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'documents', 'action' => 'delete', $document['id']), array('class' => 'btn btn-mini'), __('Are you sure you want to delete # %s?', $document['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
            </table>
            <!-- .table table-striped table-bordered -->

            <?php endif; ?>


            <div class="actions">
                <?php echo $this->Html->link('<i class="icon-plus icon-white"></i> '.__('New Document'), array('controller' => 'documents', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>            </div>
            <!-- .actions -->

        </div><!-- .related -->

    
        <div class="related">

            <h3><?php echo __('Related Events'); ?></h3>

            <?php if (!empty($location['Event'])): ?>

            <table class="table table-striped table-bordered">
                <tr>
                    		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Location Id'); ?></th>
		<th><?php echo __('Program Id'); ?></th>
		<th><?php echo __('Project Id'); ?></th>
		<th><?php echo __('Event Name'); ?></th>
		<th><?php echo __('Event Start'); ?></th>
		<th><?php echo __('Event End'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
                	<?php
								$i = 0;
								foreach ($location['Event'] as $event): ?>
		<tr>
			<td><?php echo $event['id']; ?></td>
			<td><?php echo $event['location_id']; ?></td>
			<td><?php echo $event['program_id']; ?></td>
			<td><?php echo $event['project_id']; ?></td>
			<td><?php echo $event['event_name']; ?></td>
			<td><?php echo $event['event_start']; ?></td>
			<td><?php echo $event['event_end']; ?></td>
			<td><?php echo $event['created']; ?></td>
			<td><?php echo $event['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'events', 'action' => 'view', $event['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'events', 'action' => 'edit', $event['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'events', 'action' => 'delete', $event['id']), array('class' => 'btn btn-mini'), __('Are you sure you want to delete # %s?', $event['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
            </table>
            <!-- .table table-striped table-bordered -->

            <?php endif; ?>


            <div class="actions">
                <?php echo $this->Html->link('<i class="icon-plus icon-white"></i> '.__('New Event'), array('controller' => 'events', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>            </div>
            <!-- .actions -->

        </div><!-- .related -->

    
        <div class="related">

            <h3><?php echo __('Related Inventories'); ?></h3>

            <?php if (!empty($location['Inventory'])): ?>

            <table class="table table-striped table-bordered">
                <tr>
                    		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Location Id'); ?></th>
		<th><?php echo __('Yieldtype Id'); ?></th>
		<th><?php echo __('Yield Measure Id'); ?></th>
		<th><?php echo __('Amount'); ?></th>
		<th><?php echo __('Note'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
                	<?php
								$i = 0;
								foreach ($location['Inventory'] as $inventory): ?>
		<tr>
			<td><?php echo $inventory['id']; ?></td>
			<td><?php echo $inventory['location_id']; ?></td>
			<td><?php echo $inventory['yieldtype_id']; ?></td>
			<td><?php echo $inventory['yield_measure_id']; ?></td>
			<td><?php echo $inventory['amount']; ?></td>
			<td><?php echo $inventory['note']; ?></td>
			<td><?php echo $inventory['created']; ?></td>
			<td><?php echo $inventory['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'inventories', 'action' => 'view', $inventory['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'inventories', 'action' => 'edit', $inventory['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'inventories', 'action' => 'delete', $inventory['id']), array('class' => 'btn btn-mini'), __('Are you sure you want to delete # %s?', $inventory['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
            </table>
            <!-- .table table-striped table-bordered -->

            <?php endif; ?>


            <div class="actions">
                <?php echo $this->Html->link('<i class="icon-plus icon-white"></i> '.__('New Inventory'), array('controller' => 'inventories', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>            </div>
            <!-- .actions -->

        </div><!-- .related -->

    
        <div class="related">

            <h3><?php echo __('Related Ledgers'); ?></h3>

            <?php if (!empty($location['Ledger'])): ?>

            <table class="table table-striped table-bordered">
                <tr>
                    		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Incometype Id'); ?></th>
		<th><?php echo __('Expensetype Id'); ?></th>
		<th><?php echo __('Event Id'); ?></th>
		<th><?php echo __('Program Id'); ?></th>
		<th><?php echo __('Project Id'); ?></th>
		<th><?php echo __('Location Id'); ?></th>
		<th><?php echo __('Profile Id'); ?></th>
		<th><?php echo __('Amount'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Note'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
                	<?php
								$i = 0;
								foreach ($location['Ledger'] as $ledger): ?>
		<tr>
			<td><?php echo $ledger['id']; ?></td>
			<td><?php echo $ledger['incometype_id']; ?></td>
			<td><?php echo $ledger['expensetype_id']; ?></td>
			<td><?php echo $ledger['event_id']; ?></td>
			<td><?php echo $ledger['program_id']; ?></td>
			<td><?php echo $ledger['project_id']; ?></td>
			<td><?php echo $ledger['location_id']; ?></td>
			<td><?php echo $ledger['profile_id']; ?></td>
			<td><?php echo $ledger['amount']; ?></td>
			<td><?php echo $ledger['name']; ?></td>
			<td><?php echo $ledger['note']; ?></td>
			<td><?php echo $ledger['created']; ?></td>
			<td><?php echo $ledger['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'ledgers', 'action' => 'view', $ledger['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'ledgers', 'action' => 'edit', $ledger['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'ledgers', 'action' => 'delete', $ledger['id']), array('class' => 'btn btn-mini'), __('Are you sure you want to delete # %s?', $ledger['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
            </table>
            <!-- .table table-striped table-bordered -->

            <?php endif; ?>


            <div class="actions">
                <?php echo $this->Html->link('<i class="icon-plus icon-white"></i> '.__('New Ledger'), array('controller' => 'ledgers', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>            </div>
            <!-- .actions -->

        </div><!-- .related -->

    
        <div class="related">

            <h3><?php echo __('Related Tasks'); ?></h3>

            <?php if (!empty($location['Task'])): ?>

            <table class="table table-striped table-bordered">
                <tr>
                    		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Program Id'); ?></th>
		<th><?php echo __('Project Id'); ?></th>
		<th><?php echo __('Location Id'); ?></th>
		<th><?php echo __('Profile Id'); ?></th>
		<th><?php echo __('Tasktype Id'); ?></th>
		<th><?php echo __('Task'); ?></th>
		<th><?php echo __('Task Start'); ?></th>
		<th><?php echo __('Task End'); ?></th>
		<th><?php echo __('Complete'); ?></th>
		<th><?php echo __('Note'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
                	<?php
								$i = 0;
								foreach ($location['Task'] as $task): ?>
		<tr>
			<td><?php echo $task['id']; ?></td>
			<td><?php echo $task['program_id']; ?></td>
			<td><?php echo $task['project_id']; ?></td>
			<td><?php echo $task['location_id']; ?></td>
			<td><?php echo $task['profile_id']; ?></td>
			<td><?php echo $task['tasktype_id']; ?></td>
			<td><?php echo $task['task']; ?></td>
			<td><?php echo $task['task_start']; ?></td>
			<td><?php echo $task['task_end']; ?></td>
			<td><?php echo $task['complete']; ?></td>
			<td><?php echo $task['note']; ?></td>
			<td><?php echo $task['created']; ?></td>
			<td><?php echo $task['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'tasks', 'action' => 'view', $task['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'tasks', 'action' => 'edit', $task['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'tasks', 'action' => 'delete', $task['id']), array('class' => 'btn btn-mini'), __('Are you sure you want to delete # %s?', $task['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
            </table>
            <!-- .table table-striped table-bordered -->

            <?php endif; ?>


            <div class="actions">
                <?php echo $this->Html->link('<i class="icon-plus icon-white"></i> '.__('New Task'), array('controller' => 'tasks', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>            </div>
            <!-- .actions -->

        </div><!-- .related -->

    
        <div class="related">

            <h3><?php echo __('Related Yields'); ?></h3>

            <?php if (!empty($location['Yield'])): ?>

            <table class="table table-striped table-bordered">
                <tr>
                    		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Location Id'); ?></th>
		<th><?php echo __('Yieldtype Id'); ?></th>
		<th><?php echo __('Yield Measure Id'); ?></th>
		<th><?php echo __('Yield'); ?></th>
		<th><?php echo __('Year'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
                	<?php
								$i = 0;
								foreach ($location['Yield'] as $yield): ?>
		<tr>
			<td><?php echo $yield['id']; ?></td>
			<td><?php echo $yield['location_id']; ?></td>
			<td><?php echo $yield['yieldtype_id']; ?></td>
			<td><?php echo $yield['yield_measure_id']; ?></td>
			<td><?php echo $yield['yield']; ?></td>
			<td><?php echo $yield['year']; ?></td>
			<td><?php echo $yield['created']; ?></td>
			<td><?php echo $yield['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'yields', 'action' => 'view', $yield['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'yields', 'action' => 'edit', $yield['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'yields', 'action' => 'delete', $yield['id']), array('class' => 'btn btn-mini'), __('Are you sure you want to delete # %s?', $yield['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
            </table>
            <!-- .table table-striped table-bordered -->

            <?php endif; ?>


            <div class="actions">
                <?php echo $this->Html->link('<i class="icon-plus icon-white"></i> '.__('New Yield'), array('controller' => 'yields', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>            </div>
            <!-- .actions -->

        </div><!-- .related -->

                        </div>
                    <div class="widget-foot">
                        <!-- Footer goes here -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
