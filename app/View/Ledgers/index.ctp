<?php
/**
 * Index Ledger View
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Views
 * @since         PAF v 1.0
 *
 *
 */
?><div class="matter">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="widget">
                    <div class="widget-head">
                        <div class="pull-left">
                            <h2>
                                <?php echo __('Ledgers'); ?><?php echo $this->Html->link('Add <i class="icon-plus"></i>',array('controller'=> 'ledgers','action'    => 'add'),array('escape' => false, 'class' => 'btn btn-primary btn-large offset1'));?>                            </h2>

                        </div>
                        <div class="widget-icons pull-right">
                            <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>
                            <a href="#" class="wclose"><i class="icon-remove"></i></a>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                    <div class="widget-content">
                        <div class="padd">

                            <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered">
                                <tr>
                                                                            <th><?php echo $this->Paginator->sort('id'); ?></th>
                                                                            <th><?php echo $this->Paginator->sort('incometype_id'); ?></th>
                                                                            <th><?php echo $this->Paginator->sort('expensetype_id'); ?></th>
                                                                            <th><?php echo $this->Paginator->sort('event_id'); ?></th>
                                                                            <th><?php echo $this->Paginator->sort('program_id'); ?></th>
                                                                            <th><?php echo $this->Paginator->sort('project_id'); ?></th>
                                                                            <th><?php echo $this->Paginator->sort('location_id'); ?></th>
                                                                            <th><?php echo $this->Paginator->sort('profile_id'); ?></th>
                                                                            <th><?php echo $this->Paginator->sort('amount'); ?></th>
                                                                            <th><?php echo $this->Paginator->sort('name'); ?></th>
                                                                            <th><?php echo $this->Paginator->sort('note'); ?></th>
                                                                            <th><?php echo $this->Paginator->sort('created'); ?></th>
                                                                            <th><?php echo $this->Paginator->sort('modified'); ?></th>
                                                                        <th class="actions"><?php echo __('Actions'); ?></th>
                                </tr>
                                <?php
                                    foreach ($ledgers as $ledger): ?>
	<tr>
		<td><?php echo h($ledger['Ledger']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($ledger['Incometype']['id'], array('controller' => 'incometypes', 'action' => 'view', $ledger['Incometype']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($ledger['Expensetype']['id'], array('controller' => 'expensetypes', 'action' => 'view', $ledger['Expensetype']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($ledger['Event']['id'], array('controller' => 'events', 'action' => 'view', $ledger['Event']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($ledger['Program']['name'], array('controller' => 'programs', 'action' => 'view', $ledger['Program']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($ledger['Project']['name'], array('controller' => 'projects', 'action' => 'view', $ledger['Project']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($ledger['Location']['name'], array('controller' => 'locations', 'action' => 'view', $ledger['Location']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($ledger['Profile']['id'], array('controller' => 'profiles', 'action' => 'view', $ledger['Profile']['id'])); ?>
		</td>
		<td><?php echo h($ledger['Ledger']['amount']); ?>&nbsp;</td>
		<td><?php echo h($ledger['Ledger']['name']); ?>&nbsp;</td>
		<td><?php echo h($ledger['Ledger']['note']); ?>&nbsp;</td>
		<td><?php echo h($ledger['Ledger']['created']); ?>&nbsp;</td>
		<td><?php echo h($ledger['Ledger']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $ledger['Ledger']['id']), array('class' => 'btn btn-mini')); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $ledger['Ledger']['id']), array('class' => 'btn btn-mini')); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $ledger['Ledger']['id']), array('class' => 'btn btn-mini'), __('Are you sure you want to delete # %s?', $ledger['Ledger']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
                            </table>

                            <p>
                                <small>
                                    <?php
                                    echo $this->Paginator->counter(array(
                                    'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                                    ));
                                    ?>                                </small>
                            </p>

                            <div class="pagination">
                                <ul>
                                    <?php
		echo $this->Paginator->prev('< ' . __('Previous'), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
		echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
		echo $this->Paginator->next(__('Next') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
	?>
                                </ul>
                            </div>
                            <!-- .pagination -->

                        </div>
                        <div class="widget-foot">
                            <!-- Footer goes here -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
