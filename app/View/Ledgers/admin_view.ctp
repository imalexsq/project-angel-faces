<?php
/**
 * Admin View Ledger View
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Views
 * @since         PAF v 1.0
 *
 *
 */
?>
<div class="matter">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="widget">
                    <div class="widget-head">
                        <div class="pull-left">
                            <h2>
                                <?php echo __('Ledgers'); ?><?php echo $this->Html->link('Add <i class="icon-plus"></i>',array('controller'=> 'ledgers','action'    => 'add'),array('escape' => false, 'class' => 'btn btn-primary btn-large offset1'));?>                            </h2>

                        </div>
                        <div class="widget-icons pull-right">
                            <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>
                            <a href="#" class="wclose"><i class="icon-remove"></i></a>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                    <div class="widget-content">
                        <div class="padd">

        <table class="table table-striped table-bordered">
            <tr>		<td><strong><?php echo __('Id'); ?></strong></td>
		<td>
			<?php echo h($ledger['Ledger']['id']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Incometype'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($ledger['Incometype']['id'], array('controller' => 'incometypes', 'action' => 'view', $ledger['Incometype']['id']), array('class' => '')); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Expensetype'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($ledger['Expensetype']['id'], array('controller' => 'expensetypes', 'action' => 'view', $ledger['Expensetype']['id']), array('class' => '')); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Event'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($ledger['Event']['id'], array('controller' => 'events', 'action' => 'view', $ledger['Event']['id']), array('class' => '')); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Program'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($ledger['Program']['name'], array('controller' => 'programs', 'action' => 'view', $ledger['Program']['id']), array('class' => '')); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Project'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($ledger['Project']['name'], array('controller' => 'projects', 'action' => 'view', $ledger['Project']['id']), array('class' => '')); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Location'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($ledger['Location']['name'], array('controller' => 'locations', 'action' => 'view', $ledger['Location']['id']), array('class' => '')); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Profile'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($ledger['Profile']['id'], array('controller' => 'profiles', 'action' => 'view', $ledger['Profile']['id']), array('class' => '')); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Amount'); ?></strong></td>
		<td>
			<?php echo h($ledger['Ledger']['amount']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Name'); ?></strong></td>
		<td>
			<?php echo h($ledger['Ledger']['name']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Note'); ?></strong></td>
		<td>
			<?php echo h($ledger['Ledger']['note']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Created'); ?></strong></td>
		<td>
			<?php echo h($ledger['Ledger']['created']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Modified'); ?></strong></td>
		<td>
			<?php echo h($ledger['Ledger']['modified']); ?>
			&nbsp;
		</td>
</tr>        </table>
        <!-- .table table-striped table-bordered -->

    </div>
    <!-- .view -->

                        </div>
                    <div class="widget-foot">
                        <!-- Footer goes here -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
