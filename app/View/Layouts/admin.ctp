<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 *
 * @var $this View
 */

$cakeDescription = __d('paf_dev', Configure::read('Sandbox.admin.name'));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php echo $this->Html->charset(); ?>
    <title>
        <?php echo $cakeDescription ?>:
        <?php echo $title_for_layout; ?>
    </title>
    <?php
    echo $this->Html->meta('icon');

    echo $this->Html->css('bootstrap.min');
    echo $this->Html->css('bootstrap-responsive.min');
    echo $this->Html->css('redmond/jquery-ui-1.10.3.custom.min');

    echo $this->fetch('meta');
    echo $this->fetch('css');
    ?>
</head>
<body>
<div id="container">
    <div id="header" class="row-fluid">
        <h1><?php echo $this->Html->link($cakeDescription, "http://" . Configure::read('Sandbox.admin.url')); ?></h1>
    </div>
    <div class="row-fluid">
        <div id="left-sidebar" class="span2">
            <?php echo $this->element('dev_menu') ?>
        </div>
        <div id="content" class="span9">

            <?php echo $this->Session->flash(); ?>
            <?php echo $this->Session->flash('info'); ?>

            <?php echo $this->fetch('content'); ?>
        </div>
    </div>
    <div id="footer">
        <?php echo $this->Html->link(
            $this->Html->image('cake.power.gif', array('alt' => $cakeDescription, 'border' => '0')),
            'http://www.cakephp.org/',
            array('target' => '_blank', 'escape' => false)
        );
        ?>
    </div>
</div>
<?php
echo $this->Html->script('jquery.min');
echo $this->fetch('script');
echo $this->Js->writeBuffer();
echo $this->Html->script('bootstrap.min');
?>
</body>
</html>
