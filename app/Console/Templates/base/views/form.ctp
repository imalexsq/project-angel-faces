<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Console.Templates.default.views
 * @since         CakePHP(tm) v 1.2.0.5234
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<?php echo "<?php"; ?>

/**
 * <?php printf("%s %s", Inflector::humanize($action), $singularHumanName); ?> View
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Views
 * @since         PAF v 1.0
 *
 *
 */
<?php echo "?>"; ?>
<div class="matter">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="widget">
                    <div class="widget-head">
                        <div class="pull-left">
                            <h2><?php printf("<?php echo __('%s %s'); ?>", Inflector::humanize($action), $singularHumanName); ?></h2>
                        </div>
                        <div class="widget-icons pull-right">
                            <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>
                            <a href="#" class="wclose"><i class="icon-remove"></i></a>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                    <div class="widget-content">
                        <div class="padd">

                            <?php echo "<?php echo \$this->Form->create('{$modelClass}', array('inputDefaults' => array('label' => false), 'class' => 'form form-horizontal')); ?>\n"; ?>
                            <fieldset>

                                <?php
                                foreach ($fields as $field) {
                                    if (strpos($action, 'add') !== false && $field == $primaryKey) {
                                        continue;
                                    } elseif (!in_array($field, array('created', 'modified', 'updated'))) {
                                        $ufield = Inflector::humanize($field);
                                        echo "<div class=\"control-group\">\n";
                                        echo "\t<?php echo \$this->Form->label('{$field}', '{$ufield}', array('class' => 'control-label'));?>\n";
                                        echo "\t<div class=\"controls\">\n";
                                        echo "\t\t<?php echo \$this->Form->input('{$field}', array('class' => 'span12')); ?>\n";
                                        echo "\t</div><!-- .controls -->\n";
                                        echo "</div><!-- .control-group -->\n";
                                        echo "\n";
                                    }
                                }
                                if (!empty($associations['hasAndBelongsToMany'])) {
                                    foreach ($associations['hasAndBelongsToMany'] as $assocName => $assocData) {
                                        echo "\t\t<?php echo \$this->Form->input('{$assocName}');?>\n";
                                    }
                                }
                                ?>
                            </fieldset>
                            <?php
                            echo "<?php echo \$this->Form->submit('Submit', array('class' => 'btn btn-large btn-primary')); ?>\n";
                            echo "<?php echo \$this->Form->end(); ?>\n";
                            ?>
                        </div>
                        <div class="widget-foot">
                            <!-- Footer goes here -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

