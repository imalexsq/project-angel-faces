CREATE DATABASE  IF NOT EXISTS `paf` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `paf`;
-- MySQL dump 10.13  Distrib 5.5.31, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: paf
-- ------------------------------------------------------
-- Server version	5.5.31-0ubuntu0.13.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tasks`
--

DROP TABLE IF EXISTS `tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `program_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `profile_id` int(11) NOT NULL,
  `tasktype_id` int(11) NOT NULL,
  `task` varchar(255) NOT NULL,
  `task_start` datetime DEFAULT NULL,
  `task_end` datetime DEFAULT NULL,
  `complete` tinyint(1) DEFAULT NULL,
  `note` mediumtext,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tasks_programs1_idx` (`program_id`),
  KEY `fk_tasks_projects1_idx` (`project_id`),
  KEY `fk_tasks_locations1_idx` (`location_id`),
  KEY `fk_tasks_entities1_idx` (`profile_id`),
  KEY `fk_tasks_task_types1_idx` (`tasktype_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tasks`
--

LOCK TABLES `tasks` WRITE;
/*!40000 ALTER TABLE `tasks` DISABLE KEYS */;
/*!40000 ALTER TABLE `tasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `addresses`
--

DROP TABLE IF EXISTS `addresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `addresstype_id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  `address_1` varchar(255) DEFAULT NULL,
  `address_2` varchar(255) DEFAULT NULL,
  `address_3` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(2) DEFAULT NULL,
  `zip_code` varchar(10) DEFAULT NULL,
  `note` mediumtext,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_addresses_address_types1_idx` (`addresstype_id`),
  KEY `fk_addresses_areas1_idx` (`area_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `addresses`
--

LOCK TABLES `addresses` WRITE;
/*!40000 ALTER TABLE `addresses` DISABLE KEYS */;
/*!40000 ALTER TABLE `addresses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phones`
--

DROP TABLE IF EXISTS `phones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone_type` enum('Business','Cell','Fax','Home','Work') NOT NULL,
  `phone_number` varchar(10) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phones`
--

LOCK TABLES `phones` WRITE;
/*!40000 ALTER TABLE `phones` DISABLE KEYS */;
/*!40000 ALTER TABLE `phones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `locations`
--

DROP TABLE IF EXISTS `locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `program_id` int(11) NOT NULL,
  `profile_id` int(11) NOT NULL,
  `address_id` int(11) NOT NULL,
  `fee_id` int(11) NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `note` mediumtext,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_locations_projects1_idx` (`project_id`),
  KEY `fk_locations_programs1_idx` (`program_id`),
  KEY `fk_locations_entities1_idx` (`profile_id`),
  KEY `fk_locations_addresses1_idx` (`address_id`),
  KEY `fk_locations_fees1_idx` (`fee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locations`
--

LOCK TABLES `locations` WRITE;
/*!40000 ALTER TABLE `locations` DISABLE KEYS */;
/*!40000 ALTER TABLE `locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `content` longtext NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fees`
--

DROP TABLE IF EXISTS `fees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recurs` enum('One-Time', 'Monthly', 'Quarterly', 'Weekly', 'Yearly'),
  `name` varchar(250) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fees`
--

LOCK TABLES `fees` WRITE;
/*!40000 ALTER TABLE `fees` DISABLE KEYS */;
/*!40000 ALTER TABLE `fees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `keywords`
--

DROP TABLE IF EXISTS `keywords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `keywords` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `keywords`
--

LOCK TABLES `keywords` WRITE;
/*!40000 ALTER TABLE `keywords` DISABLE KEYS */;
/*!40000 ALTER TABLE `keywords` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventories`
--

DROP TABLE IF EXISTS `inventories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location_id` int(11) NOT NULL,
  `yieldtype_id` int(11) NOT NULL,
  `yield_measure_id` int(11) NOT NULL,
  `amount` float(12,2) DEFAULT NULL,
  `note` mediumtext,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_inventories_locations1` (`location_id`),
  KEY `fk_inventories_yield_types1` (`yieldtype_id`),
  KEY `fk_inventories_yield_measures1_idx` (`yield_measure_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventories`
--

LOCK TABLES `inventories` WRITE;
/*!40000 ALTER TABLE `inventories` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aros_acos`
--

DROP TABLE IF EXISTS `aros_acos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aros_acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `aro_id` int(10) NOT NULL,
  `aco_id` int(10) NOT NULL,
  `_create` varchar(2) NOT NULL DEFAULT '0',
  `_read` varchar(2) NOT NULL DEFAULT '0',
  `_update` varchar(2) NOT NULL DEFAULT '0',
  `_delete` varchar(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ARO_ACO_KEY` (`aro_id`,`aco_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf32;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aros_acos`
--

LOCK TABLES `aros_acos` WRITE;
/*!40000 ALTER TABLE `aros_acos` DISABLE KEYS */;
INSERT INTO `aros_acos` VALUES (1,1,1,'1','1','1','1'),(11,2,46,'1','1','1','1'),(12,2,45,'1','1','1','1'),(13,2,44,'1','1','1','1'),(14,2,48,'1','1','1','1'),(15,3,48,'1','1','1','1'),(16,3,43,'1','1','1','1'),(17,2,43,'1','1','1','1'),(18,1,47,'-1','-1','-1','-1'),(23,2,42,'1','1','1','1');
/*!40000 ALTER TABLE `aros_acos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profiletypes`
--

DROP TABLE IF EXISTS `profiletypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profiletypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profiletypes`
--

LOCK TABLES `profiletypes` WRITE;
/*!40000 ALTER TABLE `profiletypes` DISABLE KEYS */;
/*!40000 ALTER TABLE `profiletypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aros`
--

DROP TABLE IF EXISTS `aros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aros` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf32;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aros`
--

LOCK TABLES `aros` WRITE;
/*!40000 ALTER TABLE `aros` DISABLE KEYS */;
INSERT INTO `aros` VALUES (1,NULL,'Group',1,NULL,1,4),(2,NULL,'Group',2,NULL,5,8),(3,NULL,'Group',3,NULL,9,12),(4,1,'User',1,NULL,2,3),(5,2,'User',2,NULL,6,7),(6,3,'User',3,NULL,10,11);
/*!40000 ALTER TABLE `aros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `composts`
--

DROP TABLE IF EXISTS `composts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `composts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) NOT NULL,
  `location_id` int(11) DEFAULT NULL,
  `yieldtype_id` int(11) DEFAULT NULL,
  `cubic_ft` int(11) DEFAULT NULL,
  `dt_received` datetime DEFAULT NULL,
  `dt_released` datetime DEFAULT NULL,
  `price` float(12,2) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `composts`
--

LOCK TABLES `composts` WRITE;
/*!40000 ALTER TABLE `composts` DISABLE KEYS */;
/*!40000 ALTER TABLE `composts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `addresstypes`
--

DROP TABLE IF EXISTS `addresstypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `addresstypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `addresstypes`
--

LOCK TABLES `addresstypes` WRITE;
/*!40000 ALTER TABLE `addresstypes` DISABLE KEYS */;
/*!40000 ALTER TABLE `addresstypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teams`
--

DROP TABLE IF EXISTS `teams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `teamtype_id` int(11) NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `teamtypes`
--

DROP TABLE IF EXISTS `teamtypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teamtypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `profiles_teams`
--

DROP TABLE IF EXISTS `profiles_teams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profiles_teams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `profiles_teams_profile_id_idx` (`profile_id`),
  KEY `profiles_teams_team_id_idx` (`team_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `keywords_posts`
--

DROP TABLE IF EXISTS `keywords_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `keywords_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keyword_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_keywords_posts_keywords1_idx` (`keyword_id`),
  KEY `fk_keywords_posts_posts1_idx` (`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `keywords_posts`
--

LOCK TABLES `keywords_posts` WRITE;
/*!40000 ALTER TABLE `keywords_posts` DISABLE KEYS */;
/*!40000 ALTER TABLE `keywords_posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `functiontypes`
--

DROP TABLE IF EXISTS `functiontypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `functiontypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `functiontypes`
--

LOCK TABLES `functiontypes` WRITE;
/*!40000 ALTER TABLE `functiontypes` DISABLE KEYS */;
/*!40000 ALTER TABLE `functiontypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `incometypes`
--

DROP TABLE IF EXISTS `incometypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `incometypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `incometypes`
--

LOCK TABLES `incometypes` WRITE;
/*!40000 ALTER TABLE `incometypes` DISABLE KEYS */;
/*!40000 ALTER TABLE `incometypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ledgers`
--

DROP TABLE IF EXISTS `ledgers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ledgers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `incometype_id` int(11) DEFAULT NULL,
  `expensetype_id` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `program_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `profile_id` int(11) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `note` varchar(250) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ledger_incometype_id_idx` (`incometype_id`),
  KEY `ledger_expensetype_id_idx` (`expensetype_id`),
  KEY `ledger_program_id_idx` (`program_id`),
  KEY `ledger_project_id_idx` (`project_id`),
  KEY `ledger_location_id_idx` (`location_id`),
  KEY `ledger_profile_id_idx` (`profile_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ledgers`
--

LOCK TABLES `ledgers` WRITE;
/*!40000 ALTER TABLE `ledgers` DISABLE KEYS */;
/*!40000 ALTER TABLE `ledgers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phones_profiles`
--

DROP TABLE IF EXISTS `phones_profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phones_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) NOT NULL,
  `phone_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_entities_phones_entities_idx` (`profile_id`),
  KEY `fk_entities_phones_phones1_idx` (`phone_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phones_profiles`
--

LOCK TABLES `phones_profiles` WRITE;
/*!40000 ALTER TABLE `phones_profiles` DISABLE KEYS */;
/*!40000 ALTER TABLE `phones_profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `scheduletypes`
--

DROP TABLE IF EXISTS `scheduletypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `scheduletypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `scheduletypes`
--

LOCK TABLES `scheduletypes` WRITE;
/*!40000 ALTER TABLE `scheduletypes` DISABLE KEYS */;
/*!40000 ALTER TABLE `scheduletypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location_id` int(11) NOT NULL,
  `program_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `event_name` varchar(250) DEFAULT NULL,
  `event_start` datetime DEFAULT NULL,
  `event_end` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_events_locations1_idx` (`location_id`),
  KEY `fk_events_programs1_idx` (`program_id`),
  KEY `fk_events_projects1_idx` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `addresses_profiles`
--

DROP TABLE IF EXISTS `addresses_profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `addresses_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) NOT NULL,
  `address_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_profiles_addresses_entities1_idx` (`profile_id`),
  KEY `fk_profiles_addresses_addresses1_idx` (`address_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `agendas`
--

DROP TABLE IF EXISTS `agendas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agendas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agendatype_id` int(11) NOT NULL,
  `event_id` int(11) NULL,
  `name` varchar(250) NOT NULL,
  `content` longtext NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `event_id_idx` (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `agendatypes`
--

DROP TABLE IF EXISTS `agendatypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agendatypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `addresses_profiles`
--

LOCK TABLES `addresses_profiles` WRITE;
/*!40000 ALTER TABLE `addresses_profiles` DISABLE KEYS */;
/*!40000 ALTER TABLE `addresses_profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(75) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`,`group_id`),
  KEY `fk_users_groups_idx` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf32;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'admin','201b69fbdffce48682028be6d41daf399c58877c','2012-11-17 00:46:44','2012-11-17 00:46:44'),(2,2,'manager','3fc433bf019786810710ce2328ce301b5382475d','2012-11-17 00:47:09','2012-11-17 00:47:09'),(3,3,'employee','97dfa6506977b8e6bcde9cae5ff98d655c5e04c8','2012-11-17 00:47:39','2012-11-17 00:47:39');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tasktypes`
--

DROP TABLE IF EXISTS `tasktypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tasktypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tasktypes`
--

LOCK TABLES `tasktypes` WRITE;
/*!40000 ALTER TABLE `tasktypes` DISABLE KEYS */;
/*!40000 ALTER TABLE `tasktypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yield_measures`
--

DROP TABLE IF EXISTS `yield_measures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yield_measures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `measure` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yield_measures`
--

LOCK TABLES `yield_measures` WRITE;
/*!40000 ALTER TABLE `yield_measures` DISABLE KEYS */;
/*!40000 ALTER TABLE `yield_measures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yieldtypes`
--

DROP TABLE IF EXISTS `yieldtypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yieldtypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yieldtypes`
--

LOCK TABLES `yieldtypes` WRITE;
/*!40000 ALTER TABLE `yieldtypes` DISABLE KEYS */;
/*!40000 ALTER TABLE `yieldtypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `expensetypes`
--

DROP TABLE IF EXISTS `expensetypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `expensetypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `expensetypes`
--

LOCK TABLES `expensetypes` WRITE;
/*!40000 ALTER TABLE `expensetypes` DISABLE KEYS */;
/*!40000 ALTER TABLE `expensetypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf32;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` VALUES (1,'admin','2012-11-17 00:36:52','2012-11-17 00:36:52'),(2,'manager','2012-11-17 00:37:08','2012-11-17 00:37:08'),(3,'employee','2012-11-17 00:37:18','2012-11-17 00:37:18');
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedules`
--

DROP TABLE IF EXISTS `schedules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) DEFAULT NULL,
  `functiontype_id` int(11) DEFAULT NULL,
  `profile_id` int(11) DEFAULT NULL,
  `scheduletype_id` int(11) NOT NULL,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedules`
--

LOCK TABLES `schedules` WRITE;
/*!40000 ALTER TABLE `schedules` DISABLE KEYS */;
/*!40000 ALTER TABLE `schedules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `description` varchar(250) NOT NULL,
  `content` longtext NOT NULL,
  `slug` varchar(250) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects`
--

DROP TABLE IF EXISTS `projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `program_id` int(11) NOT NULL,
  `projecttype_id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `note` mediumtext,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_projects_project_types1_idx` (`projecttype_id`),
  KEY `fk_projects_programs1_idx` (`program_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects`
--

LOCK TABLES `projects` WRITE;
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `areas`
--

DROP TABLE IF EXISTS `areas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `areas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `areas`
--

LOCK TABLES `areas` WRITE;
/*!40000 ALTER TABLE `areas` DISABLE KEYS */;
/*!40000 ALTER TABLE `areas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `keywords_pages`
--

DROP TABLE IF EXISTS `keywords_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `keywords_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keyword_id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `keywords_pages`
--

LOCK TABLES `keywords_pages` WRITE;
/*!40000 ALTER TABLE `keywords_pages` DISABLE KEYS */;
/*!40000 ALTER TABLE `keywords_pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `programs`
--

DROP TABLE IF EXISTS `programs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `programs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `description` mediumtext DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `programs`
--

LOCK TABLES `programs` WRITE;
/*!40000 ALTER TABLE `programs` DISABLE KEYS */;
/*!40000 ALTER TABLE `programs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `description` varchar(250) NOT NULL,
  `content` longtext NOT NULL,
  `slug` varchar(250) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts_tags`
--

DROP TABLE IF EXISTS `posts_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_posts_tags_posts1_idx` (`post_id`),
  KEY `fk_posts_tags_tags1_idx` (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts_tags`
--

LOCK TABLES `posts_tags` WRITE;
/*!40000 ALTER TABLE `posts_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `posts_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profiles`
--

DROP TABLE IF EXISTS `profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `profiletype_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `first_name` varchar(250) DEFAULT NULL,
  `last_name` varchar(250) DEFAULT NULL,
  `business_name` varchar(250) DEFAULT NULL,
  `has_car` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `profiles_profiletype_id_idx` (`profiletype_id`),
  KEY `profiles_parent_id_idx` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `profiles_programs`
--

DROP TABLE IF EXISTS `profiles_programs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profiles_programs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) NOT NULL,
  `program_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `profiles_programs_profile_id_idx` (`profile_id`),
  KEY `profiles_programs_program_id_idx` (`program_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profiles`
--

LOCK TABLES `profiles` WRITE;
/*!40000 ALTER TABLE `profiles` DISABLE KEYS */;
/*!40000 ALTER TABLE `profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projecttypes`
--

DROP TABLE IF EXISTS `projecttypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projecttypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projecttypes`
--

LOCK TABLES `projecttypes` WRITE;
/*!40000 ALTER TABLE `projecttypes` DISABLE KEYS */;
/*!40000 ALTER TABLE `projecttypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yields`
--

DROP TABLE IF EXISTS `yields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location_id` int(11) NOT NULL,
  `yieldtype_id` int(11) NOT NULL,
  `yield_measure_id` int(11) NOT NULL,
  `yield` double NOT NULL,
  `year` smallint(6) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_yields_locations1_idx` (`location_id`),
  KEY `fk_yields_yield_types1_idx` (`yieldtype_id`),
  KEY `fk_yields_yield_measure1_idx` (`yield_measure_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yields`
--

LOCK TABLES `yields` WRITE;
/*!40000 ALTER TABLE `yields` DISABLE KEYS */;
/*!40000 ALTER TABLE `yields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acos`
--

DROP TABLE IF EXISTS `acos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=561 DEFAULT CHARSET=utf32;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acos`
--

LOCK TABLES `acos` WRITE;
/*!40000 ALTER TABLE `acos` DISABLE KEYS */;
INSERT INTO `acos` VALUES (1,NULL,NULL,NULL,'controllers',1,958),(27,1,NULL,NULL,'Groups',2,13),(28,27,NULL,NULL,'index',3,4),(29,27,NULL,NULL,'view',5,6),(30,27,NULL,NULL,'add',7,8),(31,27,NULL,NULL,'edit',9,10),(32,27,NULL,NULL,'delete',11,12),(33,1,NULL,NULL,'Pages',14,35),(41,1,NULL,NULL,'Users',36,51),(42,41,NULL,NULL,'index',37,38),(43,41,NULL,NULL,'view',39,40),(44,41,NULL,NULL,'add',41,42),(45,41,NULL,NULL,'edit',43,44),(46,41,NULL,NULL,'delete',45,46),(47,41,NULL,NULL,'login',47,48),(48,41,NULL,NULL,'logout',49,50),(87,1,NULL,NULL,'Acl',52,107),(88,87,NULL,NULL,'Acl',53,58),(89,88,NULL,NULL,'index',54,55),(90,88,NULL,NULL,'admin_index',56,57),(91,87,NULL,NULL,'Acos',59,70),(92,91,NULL,NULL,'admin_index',60,61),(93,91,NULL,NULL,'admin_empty_acos',62,63),(94,91,NULL,NULL,'admin_build_acl',64,65),(95,91,NULL,NULL,'admin_prune_acos',66,67),(96,91,NULL,NULL,'admin_synchronize',68,69),(97,87,NULL,NULL,'Aros',71,106),(98,97,NULL,NULL,'admin_index',72,73),(99,97,NULL,NULL,'admin_check',74,75),(100,97,NULL,NULL,'admin_users',76,77),(101,97,NULL,NULL,'admin_update_user_role',78,79),(102,97,NULL,NULL,'admin_ajax_role_permissions',80,81),(103,97,NULL,NULL,'admin_role_permissions',82,83),(104,97,NULL,NULL,'admin_user_permissions',84,85),(105,97,NULL,NULL,'admin_empty_permissions',86,87),(106,97,NULL,NULL,'admin_clear_user_specific_permissions',88,89),(107,97,NULL,NULL,'admin_grant_all_controllers',90,91),(108,97,NULL,NULL,'admin_deny_all_controllers',92,93),(109,97,NULL,NULL,'admin_get_role_controller_permission',94,95),(110,97,NULL,NULL,'admin_grant_role_permission',96,97),(111,97,NULL,NULL,'admin_deny_role_permission',98,99),(112,97,NULL,NULL,'admin_get_user_controller_permission',100,101),(113,97,NULL,NULL,'admin_grant_user_permission',102,103),(114,97,NULL,NULL,'admin_deny_user_permission',104,105),(115,1,NULL,NULL,'DebugKit',108,115),(116,115,NULL,NULL,'ToolbarAccess',109,114),(117,116,NULL,NULL,'history_state',110,111),(118,116,NULL,NULL,'sql_explain',112,113),(119,1,NULL,NULL,'Addresses',116,137),(120,119,NULL,NULL,'index',117,118),(121,119,NULL,NULL,'view',119,120),(122,119,NULL,NULL,'add',121,122),(123,119,NULL,NULL,'edit',123,124),(124,119,NULL,NULL,'delete',125,126),(125,119,NULL,NULL,'admin_index',127,128),(126,119,NULL,NULL,'admin_view',129,130),(127,119,NULL,NULL,'admin_add',131,132),(128,119,NULL,NULL,'admin_edit',133,134),(129,119,NULL,NULL,'admin_delete',135,136),(130,1,NULL,NULL,'AddressesProfiles',138,159),(131,130,NULL,NULL,'index',139,140),(132,130,NULL,NULL,'view',141,142),(133,130,NULL,NULL,'add',143,144),(134,130,NULL,NULL,'edit',145,146),(135,130,NULL,NULL,'delete',147,148),(136,130,NULL,NULL,'admin_index',149,150),(137,130,NULL,NULL,'admin_view',151,152),(138,130,NULL,NULL,'admin_add',153,154),(139,130,NULL,NULL,'admin_edit',155,156),(140,130,NULL,NULL,'admin_delete',157,158),(141,1,NULL,NULL,'Addresstypes',160,181),(142,141,NULL,NULL,'index',161,162),(143,141,NULL,NULL,'view',163,164),(144,141,NULL,NULL,'add',165,166),(145,141,NULL,NULL,'edit',167,168),(146,141,NULL,NULL,'delete',169,170),(147,141,NULL,NULL,'admin_index',171,172),(148,141,NULL,NULL,'admin_view',173,174),(149,141,NULL,NULL,'admin_add',175,176),(150,141,NULL,NULL,'admin_edit',177,178),(151,141,NULL,NULL,'admin_delete',179,180),(152,1,NULL,NULL,'Areas',182,203),(153,152,NULL,NULL,'index',183,184),(154,152,NULL,NULL,'view',185,186),(155,152,NULL,NULL,'add',187,188),(156,152,NULL,NULL,'edit',189,190),(157,152,NULL,NULL,'delete',191,192),(158,152,NULL,NULL,'admin_index',193,194),(159,152,NULL,NULL,'admin_view',195,196),(160,152,NULL,NULL,'admin_add',197,198),(161,152,NULL,NULL,'admin_edit',199,200),(162,152,NULL,NULL,'admin_delete',201,202),(163,1,NULL,NULL,'BusinessRuleTypes',204,225),(164,163,NULL,NULL,'index',205,206),(165,163,NULL,NULL,'view',207,208),(166,163,NULL,NULL,'add',209,210),(167,163,NULL,NULL,'edit',211,212),(168,163,NULL,NULL,'delete',213,214),(169,163,NULL,NULL,'admin_index',215,216),(170,163,NULL,NULL,'admin_view',217,218),(171,163,NULL,NULL,'admin_add',219,220),(172,163,NULL,NULL,'admin_edit',221,222),(173,163,NULL,NULL,'admin_delete',223,224),(174,1,NULL,NULL,'BusinessRules',226,247),(175,174,NULL,NULL,'index',227,228),(176,174,NULL,NULL,'view',229,230),(177,174,NULL,NULL,'add',231,232),(178,174,NULL,NULL,'edit',233,234),(179,174,NULL,NULL,'delete',235,236),(180,174,NULL,NULL,'admin_index',237,238),(181,174,NULL,NULL,'admin_view',239,240),(182,174,NULL,NULL,'admin_add',241,242),(183,174,NULL,NULL,'admin_edit',243,244),(184,174,NULL,NULL,'admin_delete',245,246),(185,1,NULL,NULL,'Categories',248,269),(186,185,NULL,NULL,'index',249,250),(187,185,NULL,NULL,'view',251,252),(188,185,NULL,NULL,'add',253,254),(189,185,NULL,NULL,'edit',255,256),(190,185,NULL,NULL,'delete',257,258),(191,185,NULL,NULL,'admin_index',259,260),(192,185,NULL,NULL,'admin_view',261,262),(193,185,NULL,NULL,'admin_add',263,264),(194,185,NULL,NULL,'admin_edit',265,266),(195,185,NULL,NULL,'admin_delete',267,268),(196,1,NULL,NULL,'Comments',270,291),(197,196,NULL,NULL,'index',271,272),(198,196,NULL,NULL,'view',273,274),(199,196,NULL,NULL,'add',275,276),(200,196,NULL,NULL,'edit',277,278),(201,196,NULL,NULL,'delete',279,280),(202,196,NULL,NULL,'admin_index',281,282),(203,196,NULL,NULL,'admin_view',283,284),(204,196,NULL,NULL,'admin_add',285,286),(205,196,NULL,NULL,'admin_edit',287,288),(206,196,NULL,NULL,'admin_delete',289,290),(207,1,NULL,NULL,'Composts',292,313),(208,207,NULL,NULL,'index',293,294),(209,207,NULL,NULL,'view',295,296),(210,207,NULL,NULL,'add',297,298),(211,207,NULL,NULL,'edit',299,300),(212,207,NULL,NULL,'delete',301,302),(213,207,NULL,NULL,'admin_index',303,304),(214,207,NULL,NULL,'admin_view',305,306),(215,207,NULL,NULL,'admin_add',307,308),(216,207,NULL,NULL,'admin_edit',309,310),(217,207,NULL,NULL,'admin_delete',311,312),(218,1,NULL,NULL,'Documents',314,335),(219,218,NULL,NULL,'index',315,316),(220,218,NULL,NULL,'view',317,318),(221,218,NULL,NULL,'add',319,320),(222,218,NULL,NULL,'edit',321,322),(223,218,NULL,NULL,'delete',323,324),(224,218,NULL,NULL,'admin_index',325,326),(225,218,NULL,NULL,'admin_view',327,328),(226,218,NULL,NULL,'admin_add',329,330),(227,218,NULL,NULL,'admin_edit',331,332),(228,218,NULL,NULL,'admin_delete',333,334),(229,1,NULL,NULL,'Events',336,357),(230,229,NULL,NULL,'index',337,338),(231,229,NULL,NULL,'view',339,340),(232,229,NULL,NULL,'add',341,342),(233,229,NULL,NULL,'edit',343,344),(234,229,NULL,NULL,'delete',345,346),(235,229,NULL,NULL,'admin_index',347,348),(236,229,NULL,NULL,'admin_view',349,350),(237,229,NULL,NULL,'admin_add',351,352),(238,229,NULL,NULL,'admin_edit',353,354),(239,229,NULL,NULL,'admin_delete',355,356),(251,1,NULL,NULL,'Expensetypes',358,379),(252,251,NULL,NULL,'index',359,360),(253,251,NULL,NULL,'view',361,362),(254,251,NULL,NULL,'add',363,364),(255,251,NULL,NULL,'edit',365,366),(256,251,NULL,NULL,'delete',367,368),(257,251,NULL,NULL,'admin_index',369,370),(258,251,NULL,NULL,'admin_view',371,372),(259,251,NULL,NULL,'admin_add',373,374),(260,251,NULL,NULL,'admin_edit',375,376),(261,251,NULL,NULL,'admin_delete',377,378),(262,1,NULL,NULL,'Fees',380,401),(263,262,NULL,NULL,'index',381,382),(264,262,NULL,NULL,'view',383,384),(265,262,NULL,NULL,'add',385,386),(266,262,NULL,NULL,'edit',387,388),(267,262,NULL,NULL,'delete',389,390),(268,262,NULL,NULL,'admin_index',391,392),(269,262,NULL,NULL,'admin_view',393,394),(270,262,NULL,NULL,'admin_add',395,396),(271,262,NULL,NULL,'admin_edit',397,398),(272,262,NULL,NULL,'admin_delete',399,400),(273,1,NULL,NULL,'Functiontypes',402,423),(274,273,NULL,NULL,'index',403,404),(275,273,NULL,NULL,'view',405,406),(276,273,NULL,NULL,'add',407,408),(277,273,NULL,NULL,'edit',409,410),(278,273,NULL,NULL,'delete',411,412),(279,273,NULL,NULL,'admin_index',413,414),(280,273,NULL,NULL,'admin_view',415,416),(281,273,NULL,NULL,'admin_add',417,418),(282,273,NULL,NULL,'admin_edit',419,420),(283,273,NULL,NULL,'admin_delete',421,422),(284,1,NULL,NULL,'Incometypes',424,445),(285,284,NULL,NULL,'index',425,426),(286,284,NULL,NULL,'view',427,428),(287,284,NULL,NULL,'add',429,430),(288,284,NULL,NULL,'edit',431,432),(289,284,NULL,NULL,'delete',433,434),(290,284,NULL,NULL,'admin_index',435,436),(291,284,NULL,NULL,'admin_view',437,438),(292,284,NULL,NULL,'admin_add',439,440),(293,284,NULL,NULL,'admin_edit',441,442),(294,284,NULL,NULL,'admin_delete',443,444),(295,1,NULL,NULL,'Inventories',446,467),(296,295,NULL,NULL,'index',447,448),(297,295,NULL,NULL,'view',449,450),(298,295,NULL,NULL,'add',451,452),(299,295,NULL,NULL,'edit',453,454),(300,295,NULL,NULL,'delete',455,456),(301,295,NULL,NULL,'admin_index',457,458),(302,295,NULL,NULL,'admin_view',459,460),(303,295,NULL,NULL,'admin_add',461,462),(304,295,NULL,NULL,'admin_edit',463,464),(305,295,NULL,NULL,'admin_delete',465,466),(306,1,NULL,NULL,'Keywords',468,489),(307,306,NULL,NULL,'index',469,470),(308,306,NULL,NULL,'view',471,472),(309,306,NULL,NULL,'add',473,474),(310,306,NULL,NULL,'edit',475,476),(311,306,NULL,NULL,'delete',477,478),(312,306,NULL,NULL,'admin_index',479,480),(313,306,NULL,NULL,'admin_view',481,482),(314,306,NULL,NULL,'admin_add',483,484),(315,306,NULL,NULL,'admin_edit',485,486),(316,306,NULL,NULL,'admin_delete',487,488),(317,1,NULL,NULL,'KeywordsPosts',490,511),(318,317,NULL,NULL,'index',491,492),(319,317,NULL,NULL,'view',493,494),(320,317,NULL,NULL,'add',495,496),(321,317,NULL,NULL,'edit',497,498),(322,317,NULL,NULL,'delete',499,500),(323,317,NULL,NULL,'admin_index',501,502),(324,317,NULL,NULL,'admin_view',503,504),(325,317,NULL,NULL,'admin_add',505,506),(326,317,NULL,NULL,'admin_edit',507,508),(327,317,NULL,NULL,'admin_delete',509,510),(328,1,NULL,NULL,'Ledgers',512,533),(329,328,NULL,NULL,'index',513,514),(330,328,NULL,NULL,'view',515,516),(331,328,NULL,NULL,'add',517,518),(332,328,NULL,NULL,'edit',519,520),(333,328,NULL,NULL,'delete',521,522),(334,328,NULL,NULL,'admin_index',523,524),(335,328,NULL,NULL,'admin_view',525,526),(336,328,NULL,NULL,'admin_add',527,528),(337,328,NULL,NULL,'admin_edit',529,530),(338,328,NULL,NULL,'admin_delete',531,532),(339,1,NULL,NULL,'Locations',534,555),(340,339,NULL,NULL,'index',535,536),(341,339,NULL,NULL,'view',537,538),(342,339,NULL,NULL,'add',539,540),(343,339,NULL,NULL,'edit',541,542),(344,339,NULL,NULL,'delete',543,544),(345,339,NULL,NULL,'admin_index',545,546),(346,339,NULL,NULL,'admin_view',547,548),(347,339,NULL,NULL,'admin_add',549,550),(348,339,NULL,NULL,'admin_edit',551,552),(349,339,NULL,NULL,'admin_delete',553,554),(350,1,NULL,NULL,'Phones',556,577),(351,350,NULL,NULL,'index',557,558),(352,350,NULL,NULL,'view',559,560),(353,350,NULL,NULL,'add',561,562),(354,350,NULL,NULL,'edit',563,564),(355,350,NULL,NULL,'delete',565,566),(356,350,NULL,NULL,'admin_index',567,568),(357,350,NULL,NULL,'admin_view',569,570),(358,350,NULL,NULL,'admin_add',571,572),(359,350,NULL,NULL,'admin_edit',573,574),(360,350,NULL,NULL,'admin_delete',575,576),(361,1,NULL,NULL,'PhonesProfiles',578,599),(362,361,NULL,NULL,'index',579,580),(363,361,NULL,NULL,'view',581,582),(364,361,NULL,NULL,'add',583,584),(365,361,NULL,NULL,'edit',585,586),(366,361,NULL,NULL,'delete',587,588),(367,361,NULL,NULL,'admin_index',589,590),(368,361,NULL,NULL,'admin_view',591,592),(369,361,NULL,NULL,'admin_add',593,594),(370,361,NULL,NULL,'admin_edit',595,596),(371,361,NULL,NULL,'admin_delete',597,598),(372,1,NULL,NULL,'Posts',600,621),(373,372,NULL,NULL,'index',601,602),(374,372,NULL,NULL,'view',603,604),(375,372,NULL,NULL,'add',605,606),(376,372,NULL,NULL,'edit',607,608),(377,372,NULL,NULL,'delete',609,610),(378,372,NULL,NULL,'admin_index',611,612),(379,372,NULL,NULL,'admin_view',613,614),(380,372,NULL,NULL,'admin_add',615,616),(381,372,NULL,NULL,'admin_edit',617,618),(382,372,NULL,NULL,'admin_delete',619,620),(383,1,NULL,NULL,'PostsTags',622,643),(384,383,NULL,NULL,'index',623,624),(385,383,NULL,NULL,'view',625,626),(386,383,NULL,NULL,'add',627,628),(387,383,NULL,NULL,'edit',629,630),(388,383,NULL,NULL,'delete',631,632),(389,383,NULL,NULL,'admin_index',633,634),(390,383,NULL,NULL,'admin_view',635,636),(391,383,NULL,NULL,'admin_add',637,638),(392,383,NULL,NULL,'admin_edit',639,640),(393,383,NULL,NULL,'admin_delete',641,642),(394,1,NULL,NULL,'Profiles',644,665),(395,394,NULL,NULL,'index',645,646),(396,394,NULL,NULL,'view',647,648),(397,394,NULL,NULL,'add',649,650),(398,394,NULL,NULL,'edit',651,652),(399,394,NULL,NULL,'delete',653,654),(400,394,NULL,NULL,'admin_index',655,656),(401,394,NULL,NULL,'admin_view',657,658),(402,394,NULL,NULL,'admin_add',659,660),(403,394,NULL,NULL,'admin_edit',661,662),(404,394,NULL,NULL,'admin_delete',663,664),(405,1,NULL,NULL,'Profiletypes',666,687),(406,405,NULL,NULL,'index',667,668),(407,405,NULL,NULL,'view',669,670),(408,405,NULL,NULL,'add',671,672),(409,405,NULL,NULL,'edit',673,674),(410,405,NULL,NULL,'delete',675,676),(411,405,NULL,NULL,'admin_index',677,678),(412,405,NULL,NULL,'admin_view',679,680),(413,405,NULL,NULL,'admin_add',681,682),(414,405,NULL,NULL,'admin_edit',683,684),(415,405,NULL,NULL,'admin_delete',685,686),(416,1,NULL,NULL,'Programs',688,709),(417,416,NULL,NULL,'index',689,690),(418,416,NULL,NULL,'view',691,692),(419,416,NULL,NULL,'add',693,694),(420,416,NULL,NULL,'edit',695,696),(421,416,NULL,NULL,'delete',697,698),(422,416,NULL,NULL,'admin_index',699,700),(423,416,NULL,NULL,'admin_view',701,702),(424,416,NULL,NULL,'admin_add',703,704),(425,416,NULL,NULL,'admin_edit',705,706),(426,416,NULL,NULL,'admin_delete',707,708),(427,1,NULL,NULL,'Projects',710,731),(428,427,NULL,NULL,'index',711,712),(429,427,NULL,NULL,'view',713,714),(430,427,NULL,NULL,'add',715,716),(431,427,NULL,NULL,'edit',717,718),(432,427,NULL,NULL,'delete',719,720),(433,427,NULL,NULL,'admin_index',721,722),(434,427,NULL,NULL,'admin_view',723,724),(435,427,NULL,NULL,'admin_add',725,726),(436,427,NULL,NULL,'admin_edit',727,728),(437,427,NULL,NULL,'admin_delete',729,730),(438,1,NULL,NULL,'Projecttypes',732,753),(439,438,NULL,NULL,'index',733,734),(440,438,NULL,NULL,'view',735,736),(441,438,NULL,NULL,'add',737,738),(442,438,NULL,NULL,'edit',739,740),(443,438,NULL,NULL,'delete',741,742),(444,438,NULL,NULL,'admin_index',743,744),(445,438,NULL,NULL,'admin_view',745,746),(446,438,NULL,NULL,'admin_add',747,748),(447,438,NULL,NULL,'admin_edit',749,750),(448,438,NULL,NULL,'admin_delete',751,752),(449,1,NULL,NULL,'Tags',754,775),(450,449,NULL,NULL,'index',755,756),(451,449,NULL,NULL,'view',757,758),(452,449,NULL,NULL,'add',759,760),(453,449,NULL,NULL,'edit',761,762),(454,449,NULL,NULL,'delete',763,764),(455,449,NULL,NULL,'admin_index',765,766),(456,449,NULL,NULL,'admin_view',767,768),(457,449,NULL,NULL,'admin_add',769,770),(458,449,NULL,NULL,'admin_edit',771,772),(459,449,NULL,NULL,'admin_delete',773,774),(460,1,NULL,NULL,'Tasks',776,797),(461,460,NULL,NULL,'index',777,778),(462,460,NULL,NULL,'view',779,780),(463,460,NULL,NULL,'add',781,782),(464,460,NULL,NULL,'edit',783,784),(465,460,NULL,NULL,'delete',785,786),(466,460,NULL,NULL,'admin_index',787,788),(467,460,NULL,NULL,'admin_view',789,790),(468,460,NULL,NULL,'admin_add',791,792),(469,460,NULL,NULL,'admin_edit',793,794),(470,460,NULL,NULL,'admin_delete',795,796),(471,1,NULL,NULL,'Tasktypes',798,819),(472,471,NULL,NULL,'index',799,800),(473,471,NULL,NULL,'view',801,802),(474,471,NULL,NULL,'add',803,804),(475,471,NULL,NULL,'edit',805,806),(476,471,NULL,NULL,'delete',807,808),(477,471,NULL,NULL,'admin_index',809,810),(478,471,NULL,NULL,'admin_view',811,812),(479,471,NULL,NULL,'admin_add',813,814),(480,471,NULL,NULL,'admin_edit',815,816),(481,471,NULL,NULL,'admin_delete',817,818),(482,1,NULL,NULL,'YieldMeasures',820,841),(483,482,NULL,NULL,'index',821,822),(484,482,NULL,NULL,'view',823,824),(485,482,NULL,NULL,'add',825,826),(486,482,NULL,NULL,'edit',827,828),(487,482,NULL,NULL,'delete',829,830),(488,482,NULL,NULL,'admin_index',831,832),(489,482,NULL,NULL,'admin_view',833,834),(490,482,NULL,NULL,'admin_add',835,836),(491,482,NULL,NULL,'admin_edit',837,838),(492,482,NULL,NULL,'admin_delete',839,840),(493,1,NULL,NULL,'Yields',842,863),(494,493,NULL,NULL,'index',843,844),(495,493,NULL,NULL,'view',845,846),(496,493,NULL,NULL,'add',847,848),(497,493,NULL,NULL,'edit',849,850),(498,493,NULL,NULL,'delete',851,852),(499,493,NULL,NULL,'admin_index',853,854),(500,493,NULL,NULL,'admin_view',855,856),(501,493,NULL,NULL,'admin_add',857,858),(502,493,NULL,NULL,'admin_edit',859,860),(503,493,NULL,NULL,'admin_delete',861,862),(504,1,NULL,NULL,'Yieldtypes',864,885),(505,504,NULL,NULL,'index',865,866),(506,504,NULL,NULL,'view',867,868),(507,504,NULL,NULL,'add',869,870),(508,504,NULL,NULL,'edit',871,872),(509,504,NULL,NULL,'delete',873,874),(510,504,NULL,NULL,'admin_index',875,876),(511,504,NULL,NULL,'admin_view',877,878),(512,504,NULL,NULL,'admin_add',879,880),(513,504,NULL,NULL,'admin_edit',881,882),(514,504,NULL,NULL,'admin_delete',883,884),(515,1,NULL,NULL,'TwitterBootstrap',886,891),(516,515,NULL,NULL,'TwitterBootstrap',887,890),(517,516,NULL,NULL,'index',888,889),(518,1,NULL,NULL,'Schedules',892,913),(519,518,NULL,NULL,'index',893,894),(520,518,NULL,NULL,'view',895,896),(521,518,NULL,NULL,'add',897,898),(522,518,NULL,NULL,'edit',899,900),(523,518,NULL,NULL,'delete',901,902),(524,518,NULL,NULL,'admin_index',903,904),(525,518,NULL,NULL,'admin_view',905,906),(526,518,NULL,NULL,'admin_add',907,908),(527,518,NULL,NULL,'admin_edit',909,910),(528,518,NULL,NULL,'admin_delete',911,912),(529,1,NULL,NULL,'Scheduletypes',914,935),(530,529,NULL,NULL,'index',915,916),(531,529,NULL,NULL,'view',917,918),(532,529,NULL,NULL,'add',919,920),(533,529,NULL,NULL,'edit',921,922),(534,529,NULL,NULL,'delete',923,924),(535,529,NULL,NULL,'admin_index',925,926),(536,529,NULL,NULL,'admin_view',927,928),(537,529,NULL,NULL,'admin_add',929,930),(538,529,NULL,NULL,'admin_edit',931,932),(539,529,NULL,NULL,'admin_delete',933,934),(540,1,NULL,NULL,'KeywordsPages',936,957),(541,540,NULL,NULL,'index',937,938),(542,540,NULL,NULL,'view',939,940),(543,540,NULL,NULL,'add',941,942),(544,540,NULL,NULL,'edit',943,944),(545,540,NULL,NULL,'delete',945,946),(546,540,NULL,NULL,'admin_index',947,948),(547,540,NULL,NULL,'admin_view',949,950),(548,540,NULL,NULL,'admin_add',951,952),(549,540,NULL,NULL,'admin_edit',953,954),(550,540,NULL,NULL,'admin_delete',955,956),(551,33,NULL,NULL,'index',15,16),(552,33,NULL,NULL,'view',17,18),(553,33,NULL,NULL,'add',19,20),(554,33,NULL,NULL,'edit',21,22),(555,33,NULL,NULL,'delete',23,24),(556,33,NULL,NULL,'admin_index',25,26),(557,33,NULL,NULL,'admin_view',27,28),(558,33,NULL,NULL,'admin_add',29,30),(559,33,NULL,NULL,'admin_edit',31,32),(560,33,NULL,NULL,'admin_delete',33,34);
/*!40000 ALTER TABLE `acos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documents`
--

DROP TABLE IF EXISTS `documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location_id` int(11) DEFAULT NULL,
  `profile_id` int(11) DEFAULT NULL,
  `program_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `file` blob,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_documents_projects1_idx` (`project_id`),
  KEY `fk_documents_locations1_idx` (`location_id`),
  KEY `fk_documents_profiles1_idx` (`profile_id`),
  KEY `fk_documents_programs1_idx` (`program_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documents`
--

LOCK TABLES `documents` WRITE;
/*!40000 ALTER TABLE `documents` DISABLE KEYS */;
/*!40000 ALTER TABLE `documents` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-06-29 12:52:19