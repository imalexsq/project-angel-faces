-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 15, 2013 at 04:43 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "-00:00";

-- --------------------------------------------------------

--
-- Table structure for table `acos`
--

CREATE TABLE IF NOT EXISTS `acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf32 AUTO_INCREMENT=119 ;

--
-- Dumping data for table `acos`
--

INSERT INTO `acos` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `lft`, `rght`) VALUES
(1, NULL, NULL, NULL, 'controllers', 1, 170),
(2, 1, NULL, NULL, 'Clients', 2, 13),
(4, 2, NULL, NULL, 'index', 3, 4),
(5, 2, NULL, NULL, 'view', 5, 6),
(6, 2, NULL, NULL, 'add', 7, 8),
(7, 2, NULL, NULL, 'edit', 9, 10),
(8, 2, NULL, NULL, 'delete', 11, 12),
(9, 1, NULL, NULL, 'Domains', 14, 25),
(10, 9, NULL, NULL, 'index', 15, 16),
(11, 9, NULL, NULL, 'view', 17, 18),
(12, 9, NULL, NULL, 'add', 19, 20),
(13, 9, NULL, NULL, 'edit', 21, 22),
(14, 9, NULL, NULL, 'delete', 23, 24),
(15, 1, NULL, NULL, 'Emails', 26, 37),
(16, 15, NULL, NULL, 'index', 27, 28),
(17, 15, NULL, NULL, 'view', 29, 30),
(18, 15, NULL, NULL, 'add', 31, 32),
(19, 15, NULL, NULL, 'edit', 33, 34),
(20, 15, NULL, NULL, 'delete', 35, 36),
(21, 1, NULL, NULL, 'Ftps', 38, 49),
(22, 21, NULL, NULL, 'index', 39, 40),
(23, 21, NULL, NULL, 'view', 41, 42),
(24, 21, NULL, NULL, 'add', 43, 44),
(25, 21, NULL, NULL, 'edit', 45, 46),
(26, 21, NULL, NULL, 'delete', 47, 48),
(27, 1, NULL, NULL, 'Groups', 50, 61),
(28, 27, NULL, NULL, 'index', 51, 52),
(29, 27, NULL, NULL, 'view', 53, 54),
(30, 27, NULL, NULL, 'add', 55, 56),
(31, 27, NULL, NULL, 'edit', 57, 58),
(32, 27, NULL, NULL, 'delete', 59, 60),
(33, 1, NULL, NULL, 'Pages', 62, 65),
(34, 33, NULL, NULL, 'display', 63, 64),
(35, 1, NULL, NULL, 'Smtps', 66, 77),
(36, 35, NULL, NULL, 'index', 67, 68),
(37, 35, NULL, NULL, 'view', 69, 70),
(38, 35, NULL, NULL, 'add', 71, 72),
(39, 35, NULL, NULL, 'edit', 73, 74),
(40, 35, NULL, NULL, 'delete', 75, 76),
(41, 1, NULL, NULL, 'Users', 78, 93),
(42, 41, NULL, NULL, 'index', 79, 80),
(43, 41, NULL, NULL, 'view', 81, 82),
(44, 41, NULL, NULL, 'add', 83, 84),
(45, 41, NULL, NULL, 'edit', 85, 86),
(46, 41, NULL, NULL, 'delete', 87, 88),
(47, 41, NULL, NULL, 'login', 89, 90),
(48, 41, NULL, NULL, 'logout', 91, 92),
(81, 1, NULL, NULL, 'Accounts', 94, 105),
(82, 81, NULL, NULL, 'index', 95, 96),
(83, 81, NULL, NULL, 'view', 97, 98),
(84, 81, NULL, NULL, 'add', 99, 100),
(85, 81, NULL, NULL, 'edit', 101, 102),
(86, 81, NULL, NULL, 'delete', 103, 104),
(87, 1, NULL, NULL, 'Acl', 106, 161),
(88, 87, NULL, NULL, 'Acl', 107, 112),
(89, 88, NULL, NULL, 'index', 108, 109),
(90, 88, NULL, NULL, 'admin_index', 110, 111),
(91, 87, NULL, NULL, 'Acos', 113, 124),
(92, 91, NULL, NULL, 'admin_index', 114, 115),
(93, 91, NULL, NULL, 'admin_empty_acos', 116, 117),
(94, 91, NULL, NULL, 'admin_build_acl', 118, 119),
(95, 91, NULL, NULL, 'admin_prune_acos', 120, 121),
(96, 91, NULL, NULL, 'admin_synchronize', 122, 123),
(97, 87, NULL, NULL, 'Aros', 125, 160),
(98, 97, NULL, NULL, 'admin_index', 126, 127),
(99, 97, NULL, NULL, 'admin_check', 128, 129),
(100, 97, NULL, NULL, 'admin_users', 130, 131),
(101, 97, NULL, NULL, 'admin_update_user_role', 132, 133),
(102, 97, NULL, NULL, 'admin_ajax_role_permissions', 134, 135),
(103, 97, NULL, NULL, 'admin_role_permissions', 136, 137),
(104, 97, NULL, NULL, 'admin_user_permissions', 138, 139),
(105, 97, NULL, NULL, 'admin_empty_permissions', 140, 141),
(106, 97, NULL, NULL, 'admin_clear_user_specific_permissions', 142, 143),
(107, 97, NULL, NULL, 'admin_grant_all_controllers', 144, 145),
(108, 97, NULL, NULL, 'admin_deny_all_controllers', 146, 147),
(109, 97, NULL, NULL, 'admin_get_role_controller_permission', 148, 149),
(110, 97, NULL, NULL, 'admin_grant_role_permission', 150, 151),
(111, 97, NULL, NULL, 'admin_deny_role_permission', 152, 153),
(112, 97, NULL, NULL, 'admin_get_user_controller_permission', 154, 155),
(113, 97, NULL, NULL, 'admin_grant_user_permission', 156, 157),
(114, 97, NULL, NULL, 'admin_deny_user_permission', 158, 159),
(115, 1, NULL, NULL, 'DebugKit', 162, 169),
(116, 115, NULL, NULL, 'ToolbarAccess', 163, 168),
(117, 116, NULL, NULL, 'history_state', 164, 165),
(118, 116, NULL, NULL, 'sql_explain', 166, 167);

-- --------------------------------------------------------

--
-- Table structure for table `aros`
--

CREATE TABLE IF NOT EXISTS `aros` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf32 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `aros`
--

INSERT INTO `aros` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `lft`, `rght`) VALUES
(1, NULL, 'Group', 1, NULL, 1, 4),
(2, NULL, 'Group', 2, NULL, 5, 8),
(3, NULL, 'Group', 3, NULL, 9, 12),
(4, 1, 'User', 1, NULL, 2, 3),
(5, 2, 'User', 2, NULL, 6, 7),
(6, 3, 'User', 3, NULL, 10, 11);

-- --------------------------------------------------------

--
-- Table structure for table `aros_acos`
--

CREATE TABLE IF NOT EXISTS `aros_acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `aro_id` int(10) NOT NULL,
  `aco_id` int(10) NOT NULL,
  `_create` varchar(2) NOT NULL DEFAULT '0',
  `_read` varchar(2) NOT NULL DEFAULT '0',
  `_update` varchar(2) NOT NULL DEFAULT '0',
  `_delete` varchar(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ARO_ACO_KEY` (`aro_id`,`aco_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf32 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `aros_acos`
--

INSERT INTO `aros_acos` (`id`, `aro_id`, `aco_id`, `_create`, `_read`, `_update`, `_delete`) VALUES
(1, 1, 1, '1', '1', '1', '1'),
(2, 2, 6, '1', '1', '1', '1'),
(3, 2, 8, '1', '1', '1', '1'),
(4, 2, 7, '1', '1', '1', '1'),
(5, 2, 4, '1', '1', '1', '1'),
(6, 2, 5, '1', '1', '1', '1'),
(7, 3, 5, '1', '1', '1', '1'),
(8, 3, 4, '1', '1', '1', '1'),
(9, 3, 34, '1', '1', '1', '1'),
(10, 2, 34, '1', '1', '1', '1'),
(11, 2, 46, '1', '1', '1', '1'),
(12, 2, 45, '1', '1', '1', '1'),
(13, 2, 44, '1', '1', '1', '1'),
(14, 2, 48, '1', '1', '1', '1'),
(15, 3, 48, '1', '1', '1', '1'),
(16, 3, 43, '1', '1', '1', '1'),
(17, 2, 43, '1', '1', '1', '1'),
(18, 1, 47, '-1', '-1', '-1', '-1'),
(23, 2, 42, '1', '1', '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf32 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `created`, `modified`) VALUES
(1, 'admin', '2012-11-17 00:36:52', '2012-11-17 00:36:52'),
(2, 'manager', '2012-11-17 00:37:08', '2012-11-17 00:37:08'),
(3, 'employee', '2012-11-17 00:37:18', '2012-11-17 00:37:18');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(75) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`,`group_id`),
  KEY `fk_users_groups_idx` (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf32 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `group_id`, `username`, `password`, `created`, `modified`) VALUES
(1, 1, 'admin', '28bcb9e2a92eaa8d3b904be9d61bad68e5b68bc5', '2012-11-17 00:46:44', '2012-11-17 00:46:44'),
(2, 2, 'manager', '3fc433bf019786810710ce2328ce301b5382475d', '2012-11-17 00:47:09', '2012-11-17 00:47:09'),
(3, 3, 'employee', '97dfa6506977b8e6bcde9cae5ff98d655c5e04c8', '2012-11-17 00:47:39', '2012-11-17 00:47:39');
